<?php

namespace UnicaenAutoform\Form\Categorie;


use Psr\Container\ContainerInterface;

class CategorieHydratorFactory
{

    public function __invoke(ContainerInterface $container): CategorieHydrator
    {
        $hydrator = new CategorieHydrator();
        return $hydrator;
    }
}