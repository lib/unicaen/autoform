<?php

namespace UnicaenAutoform\Form\Categorie;

use UnicaenAutoform\Entity\Db\Categorie;
use Laminas\Hydrator\HydratorInterface;

class CategorieHydrator implements HydratorInterface {

    public function extract(object $object) : array
    {
        /** @var Categorie $object */
        $data = [
            'libelle' => $object->getLibelle(),
        ];
        return $data;
    }

    public function hydrate(array $data, object $object): object
    {
        /** @var Categorie $object */
        $object->setLibelle($data['libelle']);
        return $object;
    }


}