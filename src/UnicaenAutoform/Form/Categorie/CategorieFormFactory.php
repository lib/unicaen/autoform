<?php

namespace UnicaenAutoform\Form\Categorie;

use Psr\Container\ContainerExceptionInterface;
use Psr\Container\ContainerInterface;
use Psr\Container\NotFoundExceptionInterface;

class CategorieFormFactory
{

    /**
     * @param ContainerInterface $container
     * @return CategorieForm
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container): CategorieForm
    {
        /** @var CategorieHydrator $hydrator */
        $hydrator = $container->get('HydratorManager')->get(CategorieHydrator::class);

        $form = new CategorieForm();
        $form->setHydrator($hydrator);
        $form->init();

        return $form;
    }
}