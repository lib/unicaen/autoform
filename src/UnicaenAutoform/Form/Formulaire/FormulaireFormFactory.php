<?php

namespace UnicaenAutoform\Form\Formulaire;

use Psr\Container\ContainerExceptionInterface;
use Psr\Container\ContainerInterface;
use Psr\Container\NotFoundExceptionInterface;

class FormulaireFormFactory {

    /**
     * @param ContainerInterface $container
     * @return FormulaireForm
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container): FormulaireForm
    {
        /** @var FormulaireHydrator $hydrator */
        $hydrator = $container->get('HydratorManager')->get(FormulaireHydrator::class);

        $form = new FormulaireForm();
        $form->setHydrator($hydrator);
        $form->init();

        return $form;
    }
}