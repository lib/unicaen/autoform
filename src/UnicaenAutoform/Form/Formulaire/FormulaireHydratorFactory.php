<?php

namespace UnicaenAutoform\Form\Formulaire;


use Psr\Container\ContainerInterface;

class FormulaireHydratorFactory
{

    /**
     * @param ContainerInterface $container
     * @return FormulaireHydrator
     */
    public function __invoke(ContainerInterface $container): FormulaireHydrator
    {
        $hydrator = new FormulaireHydrator();
        return $hydrator;
    }
}