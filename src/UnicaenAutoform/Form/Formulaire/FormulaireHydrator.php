<?php

namespace UnicaenAutoform\Form\Formulaire;

use UnicaenAutoform\Entity\Db\Formulaire;
use Laminas\Hydrator\HydratorInterface;

class FormulaireHydrator implements HydratorInterface {

    /**
     * @param Formulaire $object
     * @return array
     */
    public function extract(object $object) : array
    {
        $data = [
            'libelle' => $object->getLibelle(),
            'description' => $object->getDescription(),
            'code' => $object->getCode(),
        ];
        return $data;
    }

    /**
     * @param array $data
     * @param Formulaire $object
     * @return Formulaire
     */
    public function hydrate(array $data, object $object): object
    {
        $object->setLibelle($data['libelle']);
        $object->setDescription($data['description']?:null);
        $object->setCode($data['code']?:null);
        return $object;
    }


}