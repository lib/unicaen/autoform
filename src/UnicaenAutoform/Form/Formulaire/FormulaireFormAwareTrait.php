<?php

namespace UnicaenAutoform\Form\Formulaire;

trait FormulaireFormAwareTrait {

    private FormulaireForm $formulaireForm;

    public function getFormulaireForm(): FormulaireForm
    {
        return $this->formulaireForm;
    }

    public function setFormulaireForm(FormulaireForm $formulaireForm): void
    {
        $this->formulaireForm = $formulaireForm;
    }


}