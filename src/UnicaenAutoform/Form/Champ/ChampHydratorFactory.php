<?php

namespace UnicaenAutoform\Form\Champ;


use Psr\Container\ContainerExceptionInterface;
use Psr\Container\ContainerInterface;
use Psr\Container\NotFoundExceptionInterface;
use UnicaenAutoform\Service\ChampType\ChampTypeService;

class ChampHydratorFactory
{

    /**
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container): ChampHydrator
    {
        /** @var ChampTypeService $champTypeService */
        $champTypeService = $container->get(ChampTypeService::class);

        $hydrator = new ChampHydrator();
        $hydrator->setChampTypeService($champTypeService);

        return $hydrator;
    }
}