<?php

namespace UnicaenAutoform\Form\Champ;

use UnicaenAutoform\Entity\Db\Champ;
use Laminas\Hydrator\HydratorInterface;
use UnicaenAutoform\Service\ChampType\ChampTypeServiceAwareTrait;

class ChampHydrator implements HydratorInterface {
    use ChampTypeServiceAwareTrait;

    public function extract(object $object) : array
    {
        /** @var Champ $object */
        $data = [
            'type'      => $object->getType()?->getCode(),
            'libelle'   => $object->getLibelle(),
            'texte'     => $object->getTexte(),
            'options'   => $object->getOptions(),
            'obligatoire'   => $object->isObligatoire(),
        ];
        return $data;
    }

    public function hydrate(array $data, object $object): object
    {
        $obligatoire = (isset($data['obligatoire']) AND $data['obligatoire'] === "1");
        $type = isset($data['type']) ? $this->getChampTypeService()->getChampType($data['type']) : null;


        /** @var Champ $object */
        $object->setType($type);
        $object->setLibelle($data['libelle']);
        $object->setTexte($data['texte']);
        $object->setOptions($data['options']);
        $object->setObligatoire($obligatoire);
        return $object;
    }


}