<?php

namespace UnicaenAutoform\Form\Champ;

trait ChampFormAwareTrait {

    private ChampForm $champForm;

    public function getChampForm(): ChampForm
    {
        return $this->champForm;
    }

    public function setChampForm(ChampForm $champForm): void
    {
        $this->champForm = $champForm;
    }


}