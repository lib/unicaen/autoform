<?php 

namespace UnicaenAutoform\Form\Champ;

use Laminas\Form\Element\Checkbox;
use Laminas\Form\Element\Button;
use Laminas\Form\Element\Select;
use Laminas\Form\Element\Text;
use Laminas\Form\Form;
use Laminas\InputFilter\Factory;
use UnicaenAutoform\Service\ChampType\ChampTypeServiceAwareTrait;

class ChampForm extends Form {
    use ChampTypeServiceAwareTrait;

    public function init(): void
    {
        // type
        $this->add([
            'type' => Select::class,
            'name' => 'type',
            'options' => [
                'label' => "Type de mise en forme <span class='icon icon-obligatoire' title='Champ obligatoire'></span> :",
                'label_options' => [ 'disable_html_escape' => true, ],
                'value_options' => $this->getChampTypeService()->getChampTypesAsOptions(),
            ],
            'attributes' => [
                'id' => 'type',
            ],
        ]);
        //libelle
        $this->add([
            'type' => Text::class,
            'name' => 'libelle',
            'options' => [
                'label' => "Libelle <span class='icon icon-obligatoire' title='Champ obligatoire'></span> :",
                'label_options' => [ 'disable_html_escape' => true, ],
            ],
            'attributes' => [
                'id' => 'libelle',
            ],
        ]);
        //libelle
        $this->add([
            'type' => Checkbox::class,
            'name' => 'obligatoire',
            'options' => [
                'label' => "Est un champ obligatoire",
            ],
            'attributes' => [
                'id' => 'obligatoire',
            ],
        ]);
        // options
        $this->add([
            'type' => Text::class,
            'name' => 'options',
            'options' => [
                'label' => "Options (séparer les options avec des ';') :",
            ],
            'attributes' => [
                'id' => 'texte',
            ],
        ]);
        // texte
        $this->add([
            'type' => Text::class,
            'name' => 'texte',
            'options' => [
                'label' => "Texte :",
            ],
            'attributes' => [
                'id' => 'texte',
            ],
        ]);
        //bouton
        $this->add([
            'type' => Button::class,
            'name' => 'creer',
            'options' => [
                'label' => '<i class="fas fa-save"></i> Enregistrer la champ',
                'label_options' => [
                    'disable_html_escape' => true,
                ],
            ],
            'attributes' => [
                'type' => 'submit',
                'class' => 'btn btn-success',
            ],
        ]);

        $this->setInputFilter((new Factory())->createInputFilter([
            'type' => [
                'required' => true,
            ],
            'libelle' => [ 'required' => true, ],
            'obligatoire' => [ 'required' => false, ],
            'texte' => [
                'required' => false,
            ],
        ]));
    }
}



