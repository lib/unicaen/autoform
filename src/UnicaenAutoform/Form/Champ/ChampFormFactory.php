<?php

namespace UnicaenAutoform\Form\Champ;

use Psr\Container\ContainerExceptionInterface;
use Psr\Container\ContainerInterface;
use Psr\Container\NotFoundExceptionInterface;
use UnicaenAutoform\Service\ChampType\ChampTypeService;

class ChampFormFactory {

    /**
     * @param ContainerInterface $container
     * @return ChampForm
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container): ChampForm
    {
        /**
         * @var ChampTypeService $champTypeService
         * @var ChampHydrator $hydrator
         **/
        $champTypeService = $container->get(ChampTypeService::class);
        $hydrator = $container->get('HydratorManager')->get(ChampHydrator::class);

        $form = new ChampForm();
        $form->setChampTypeService($champTypeService);
        $form->setHydrator($hydrator);
        $form->init();

        return $form;
    }
}