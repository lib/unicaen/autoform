<?php

namespace UnicaenAutoform\Form\ChampType;

trait ChampTypeFormAwareTrait {

    private ChampTypeForm $champTypeForm;

    public function getChampTypeForm(): ChampTypeForm
    {
        return $this->champTypeForm;
    }

    public function setChampTypeForm(ChampTypeForm $champTypeForm): void
    {
        $this->champTypeForm = $champTypeForm;
    }

}