<?php

namespace UnicaenAutoform\Form\ChampType;

use Psr\Container\ContainerExceptionInterface;
use Psr\Container\ContainerInterface;
use Psr\Container\NotFoundExceptionInterface;

class ChampTypeFormFactory {

    /**
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container): ChampTypeForm
    {
        /** @var ChampTypeHydrator $hydrator  */
        $hydrator = $container->get('HydratorManager')->get(ChampTypeHydrator::class);

        $form = new ChampTypeForm();
        $form->setHydrator($hydrator);
        return $form;
    }
}