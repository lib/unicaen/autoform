<?php

namespace UnicaenAutoform\Form\ChampType;

use Laminas\Form\Element\Button;
use Laminas\Form\Element\Text;
use Laminas\Form\Element\Textarea;
use Laminas\Form\Form;
use Laminas\InputFilter\Factory;

class ChampTypeForm extends Form
{
    public function init(): void
    {
        //libelle
        $this->add([
            'type' => Text::class,
            'name' => 'libelle',
            'options' => [
                'label' => "Libelle <span class='icon icon-obligatoire' title='Champ obligatoire'></span>:",
                'label_options' => [ 'disable_html_escape' => true, ],
            ],
            'attributes' => [
                'id' => 'libelle',
            ],
        ]);
        //description
        $this->add([
            'name' => 'description',
            'type' => Textarea::class,
            'options' => [
                'label' => 'Description : ',
                'label_attributes' => [
                    'class' => 'control-label',
                ],
            ],
            'attributes' => [
                'class' => 'tinymce form-control',
                'id' => 'description',
            ]
        ]);
        //description
        $this->add([
            'name' => 'usage',
            'type' => Textarea::class,
            'options' => [
                'label' => 'Paramètrage du type de champ : ',
                'label_attributes' => [
                    'class' => 'control-label',
                ],
            ],
            'attributes' => [
                'class' => 'tinymce form-control',
                'id' => 'usage',
            ]
        ]);
        //button
        $this->add([
            'type' => Button::class,
            'name' => 'creer',
            'options' => [
                'label' => '<i class="fas fa-save"></i> Enregistrer',
                'label_options' => [
                    'disable_html_escape' => true,
                ],
            ],
            'attributes' => [
                'type' => 'submit',
                'class' => 'btn btn-success',
            ],
        ]);
        //input filter
        $this->setInputFilter((new Factory())->createInputFilter([
            'libelle'         => [ 'required' => true,  ],
            'description'     => [ 'required' => false,  ],
            'usage'     => [ 'required' => false,  ],
        ]));
    }
}