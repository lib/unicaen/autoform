<?php

namespace UnicaenAutoform\Form\ChampType;

use Psr\Container\ContainerInterface;

class ChampTypeHydratorFactory
{

    public function __invoke(ContainerInterface $container): ChampTypeHydrator
    {
        $hydrator = new ChampTypeHydrator();
        return $hydrator;
    }
}