<?php

namespace UnicaenAutoform\Form\ChampType;

use Laminas\Hydrator\HydratorInterface;
use UnicaenAutoform\Entity\Db\ChampType;

class ChampTypeHydrator implements HydratorInterface {

    public function extract(object $object): array
    {
        /** @var ChampType $object */
        $data = [
            'libelle' => $object->getLibelle(),
            'description' => $object->getDescription(),
            'usage' => $object->getUsage(),
        ];
        return $data;
    }

    public function hydrate(array $data, object $object): object
    {
        $libelle = (isset($data['libelle']) AND trim($data['libelle']) !== '') ? trim($data['libelle']) : null;
        $description = (isset($data['description']) AND trim($data['description']) !== '') ? trim($data['description']) : null;
        $usage = (isset($data['usage']) AND trim($data['usage']) !== '') ? trim($data['usage']) : null;

        /** @var ChampType $object */
        $object->setLibelle($libelle);
        $object->setDescription($description);
        $object->setUsage($usage);
        return $object;
    }
}