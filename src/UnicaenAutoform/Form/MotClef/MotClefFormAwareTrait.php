<?php

namespace UnicaenAutoform\Form\MotClef;

trait MotClefFormAwareTrait {

    private MotClefForm $motClefForm;

    public function getMotClefForm(): MotClefForm
    {
        return $this->motClefForm;
    }

    public function setMotClefForm(MotClefForm $motClefForm): void
    {
        $this->motClefForm = $motClefForm;
    }


}