<?php

namespace UnicaenAutoform\Form\MotClef;

use Psr\Container\ContainerExceptionInterface;
use Psr\Container\ContainerInterface;
use Psr\Container\NotFoundExceptionInterface;

class MotClefFormFactory
{
    /**
     * @param ContainerInterface $container
     * @return MotClefForm
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container) : MotClefForm
    {
        /** @var MotClefHydrator $hydrator */
        $hydrator = $container->get('HydratorManager')->get(MotClefHydrator::class);

        $form = new MotClefForm();
        $form->setHydrator($hydrator);
        return $form;
    }
}