<?php

namespace UnicaenAutoform\Form\MotClef;

use Psr\Container\ContainerInterface;

class MotClefHydratorFactory
{
    public function __invoke(ContainerInterface $container): MotClefHydrator
    {
        $hydrator = new MotClefHydrator();
        return $hydrator;
    }
}