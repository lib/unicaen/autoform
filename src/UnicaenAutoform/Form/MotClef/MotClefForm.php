<?php

namespace UnicaenAutoform\Form\MotClef;

use Laminas\Form\Element\Button;
use Laminas\Form\Element\Text;
use Laminas\Form\Form;
use Laminas\InputFilter\Factory;

class MotClefForm extends Form
{
    public function init(): void
    {

        $this->add([
            'type' => Text::class,
            'name' => 'motsclefs',
            'options' => [
                'label' => "Mots-clefs :",
                'label_options' => [ 'disable_html_escape' => true, ],
            ],
            'attributes' => [
                'id' => 'libelle',
            ],
        ]);
        // button
        $this->add([
            'type' => Button::class,
            'name' => 'creer',
            'options' => [
                'label' => '<i class="fas fa-save"></i> Modifier les mots-clefs',
                'label_options' => [
                    'disable_html_escape' => true,
                ],
            ],
            'attributes' => [
                'type' => 'submit',
                'class' => 'btn btn-primary',
            ],
        ]);

        //inputFIlter
        $this->setInputFilter((new Factory())->createInputFilter([
            'motsclefs'          => [ 'required' => false,  ],
        ]));
    }
}