<?php

namespace UnicaenAutoform\Form\MotClef;

use Laminas\Hydrator\HydratorInterface;
use UnicaenAutoform\Entity\Db\Categorie;
use UnicaenAutoform\Entity\Db\Champ;
use UnicaenAutoform\Entity\HasMotsClefsInterface;

class MotClefHydrator implements HydratorInterface
{
    public function extract(object $object): array
    {
        /** @var Categorie|Champ $object */
        $data = [
            'motsclefs' => $object->getMotsClefs(),
        ];
        return $data;
    }

    public function hydrate(array $data, object $object) : object
    {
        /** @var Categorie|Champ $object */
        $motsclefs = (isset($data['motsclefs']) AND trim($data['motsclefs']) !== '')?trim($data['motsclefs']):null;
        $object->setMotsClefs($motsclefs);
        return $object;
    }

}