<?php

namespace UnicaenAutoform\Entity;

trait HasMotsClefsAwareTrait {

    private ?string $motsClefs = null;

    public function getMotsClefs(): ?string
    {
        return $this->motsClefs;
    }

    public function setMotsClefs(?string $motsclefs = null): void
    {
        $this->motsClefs = $motsclefs;
    }

    public function hasMotsClefs(array $mots) : bool
    {
        $motsClefs = explode(';', $this->getMotsClefs());
        foreach ($mots as $mot) {
            if (!in_array($mot, $motsClefs)) return false;
        }
        return true;
    }



}