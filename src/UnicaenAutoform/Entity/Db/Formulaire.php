<?php

namespace UnicaenAutoform\Entity\Db;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use UnicaenUtilisateur\Entity\Db\HistoriqueAwareInterface;
use UnicaenUtilisateur\Entity\Db\HistoriqueAwareTrait;

class Formulaire implements HistoriqueAwareInterface {
    use HistoriqueAwareTrait;

    private ?int $id = null;
    private ?string $libelle = null;
    private ?string $description = null;
    private ?string $code = null;
    private Collection $categories;

    public function __construct()
    {
        $this->categories = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLibelle(): ?string
    {
        return $this->libelle;
    }

    public function setLibelle(?string $libelle): void
    {
        $this->libelle = $libelle;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): void
    {
        $this->description = $description;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(?string $code): void
    {
        $this->code = $code;
    }

    /** @return Categorie[] */
    public function getCategories(): array
    {
        return $this->categories->toArray();
    }

    public function addCategorie(Categorie $categorie): void
    {
        $this->categories->add($categorie);
    }

    public function removeCategorie(Categorie $categorie): void
    {
        $this->categories->removeElement($categorie);
    }

    public function hasCategorie(Categorie $categorie): bool
    {
        return $this->categories->contains($categorie);
    }
}