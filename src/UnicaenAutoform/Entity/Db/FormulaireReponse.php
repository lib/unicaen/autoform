<?php

namespace UnicaenAutoform\Entity\Db;

use UnicaenUtilisateur\Entity\Db\HistoriqueAwareInterface;
use UnicaenUtilisateur\Entity\Db\HistoriqueAwareTrait;

class FormulaireReponse implements HistoriqueAwareInterface
{
    use HistoriqueAwareTrait;

    private ?int $id = null;
    private ?FormulaireInstance $instance = null;
    private ?Champ $champ = null;
    private ?string $reponse = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFormulaireInstance(): ?FormulaireInstance
    {
        return $this->instance;
    }

    public function setFormulaireInstance(?FormulaireInstance $instance): void
    {
        $this->instance = $instance;
    }

    public function getChamp(): ?Champ
    {
        return $this->champ;
    }

    public function setChamp(?Champ $champ): void
    {
        $this->champ = $champ;
    }

    public function getReponse(): ?string
    {
        return $this->reponse;
    }

    public function setReponse(?string $reponse): void
    {
        $this->reponse = $reponse;
    }

}