<?php

namespace UnicaenAutoform\Entity\Db;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

class ChampType {

    public ?string $code = null;
    public ?string $libelle = null;
    public ?string $description = null;
    public ?string $usage = null;
    public ?string $exempleLibelle = null;
    public ?string $exempleOptions = null;
    public ?string $exempleTexte = null;
    public ?string $exempleReponse = null;

    public Collection $champs;

    public function __construct()
    {
        $this->champs = new ArrayCollection();
    }

    public function getId(): ?string
    {
        return $this->getCode();
    }
    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(?string $code): void
    {
        $this->code = $code;
    }

    public function getLibelle(): ?string
    {
        return $this->libelle;
    }

    public function setLibelle(?string $libelle): void
    {
        $this->libelle = $libelle;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): void
    {
        $this->description = $description;
    }

    public function getExempleLibelle(): ?string
    {
        return $this->exempleLibelle;
    }

    public function setExempleLibelle(?string $exempleLibelle): void
    {
        $this->exempleLibelle = $exempleLibelle;
    }

    public function getExempleOptions(): ?string
    {
        return $this->exempleOptions;
    }

    public function setExempleOptions(?string $exempleOptions): void
    {
        $this->exempleOptions = $exempleOptions;
    }

    public function getExempleTexte(): ?string
    {
        return $this->exempleTexte;
    }

    public function setExempleTexte(?string $exempleTexte): void
    {
        $this->exempleTexte = $exempleTexte;
    }

    public function getExempleReponse(): ?string
    {
        return $this->exempleReponse;
    }

    public function setExempleReponse(?string $exempleReponse): void
    {
        $this->exempleReponse = $exempleReponse;
    }



    public function getUsage(): ?string
    {
        return $this->usage;
    }

    public function setUsage(?string $usage): void
    {
        $this->usage = $usage;
    }

    /** @return Champ[] */
    public function getChamps(bool $withHisto = false): array
    {
        $result = $this->champs->toArray();
        if (!$withHisto)  $result = array_filter($result, function (Champ $item) { return $item->estNonHistorise();});
        return $result;
    }
}