<?php

namespace UnicaenAutoform\Entity\Db;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use UnicaenUtilisateur\Entity\Db\HistoriqueAwareInterface;
use UnicaenUtilisateur\Entity\Db\HistoriqueAwareTrait;

class FormulaireInstance implements HistoriqueAwareInterface {
    use HistoriqueAwareTrait;

    private  ?int $id = null;
    private  ?Formulaire $formulaire = null;
    private Collection $reponses;
    private Collection $validations;

    public function __construct()
    {
        $this->reponses = new ArrayCollection();
        $this->validations = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $id): void
    {
        $this->id = $id;
    }

    public function getFormulaire(): Formulaire
    {
        return $this->formulaire;
    }

    public function setFormulaire(?Formulaire $formulaire): void
    {
        $this->formulaire = $formulaire;
    }

    /** @return FormulaireReponse[] */
    public function getReponses(): array
    {
        return $this->reponses->toArray();
    }

    public function addReponse(FormulaireReponse $reponse): void
    {
        $this->reponses->add($reponse);
    }

    public function removeReponse(FormulaireReponse $reponse): void
    {
        $this->reponses->removeElement($reponse);
    }

    public function getChamp(?int $to): ?Champ
    {
        foreach ($this->getFormulaire()->getCategories() as $categorie) {
            foreach ($categorie->getChamps() as $champ) {
                if ($champ->getId() === $to) return $champ;
            }
        }
        return null;
    }

    public function getReponseFor(Champ $champ) : ?string
    {
        /** @var FormulaireReponse $reponse */
        foreach ($this->reponses as $reponse) {
            if ($reponse->estNonHistorise() AND $reponse->getChamp()->getId() === $champ->getId())
                return $reponse->getReponse();
        }
        return null;
    }

    public function prettyPrint() : string
    {
        $text = "";
        $categories = $this->getFormulaire()->getCategories();
        $categories = array_filter($categories, function (Categorie $a) { return $a->estNonHistorise(); });
        usort($categories, function (Categorie $a, Categorie $b) { return $a->getOrdre() <=> $b->getOrdre(); });

        foreach ($categories as $categorie) {
            $as = false;
            $subtext = "<h3>".$categorie->getLibelle()."</h3>";

            $champs = $categorie->getChamps();
            $champs = array_filter($champs, function (Champ $a) { return $a->estNonHistorise(); });
            usort($champs, function (Champ $a, Champ $b) { return $a->getOrdre() <=> $b->getOrdre(); });

            foreach ($champs as $champ) {
                $reponse = $this->getReponseFor($champ);
                //todo utiliser les VHs
                if ($reponse !== null) {
                    $subtext .= '<u>'.$champ->getLibelle() . " :</u> ". $reponse . "<br>";
                    $as = true;
                }
            }

            if ($as) $text .= $subtext;
        }
        return $text;
    }

    public function fetchChampReponseByMotsClefs(array $mots) : string
    {
        foreach ($this->getReponses() as $reponse) {
            if ($reponse->getChamp()->hasMotsClefs($mots) AND $reponse->estNonHistorise()) {
                if ($reponse->getChamp()->getType()->getCode() !== 'Multiple') return $reponse->getReponse();
                else {
                    return str_replace('on_','',$reponse->getReponse());
                }
            }
        }
        return "";
    }

    public function fetchChampsReponseByMotsClefs(array $mots) : string
    {
        $texte = "";
        $responses = $this->getReponses();
        usort($responses, function (FormulaireReponse $a, FormulaireReponse $b) { return $a->getChamp()->getOrdre() <=> $b->getChamp()->getOrdre();});

        foreach ($responses as $reponse) {
            if ($reponse->getChamp()->hasMotsClefs($mots) AND $reponse->estNonHistorise()) {
                if ($texte !== "") $texte .= "<br/>";
                $texte .= $reponse->getReponse();
            }
        }
        return $texte;
    }

    /** @return FormulaireReponse[] */
    public function buildResponseDictionnary() : array
    {
        $reponses = $this->getReponses();
        $data = [];
        foreach ($reponses as $reponse) {
            $data[$reponse->getChamp()->getId()] = $reponse;
        }
        return $data;
    }

}