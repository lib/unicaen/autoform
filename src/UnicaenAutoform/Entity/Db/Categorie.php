<?php

namespace UnicaenAutoform\Entity\Db;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use UnicaenAutoform\Entity\HasMotsClefsAwareTrait;
use UnicaenAutoform\Entity\HasMotsClefsInterface;
use UnicaenUtilisateur\Entity\Db\HistoriqueAwareInterface;
use UnicaenUtilisateur\Entity\Db\HistoriqueAwareTrait;

class Categorie implements HistoriqueAwareInterface, HasMotsClefsInterface
{
    use HistoriqueAwareTrait;
    use HasMotsClefsAwareTrait;

    private ?int $id = null;
    private ?string $code = null;
    private ?string $libelle = null;
    private ?int $ordre = null;
    private ?Formulaire $formulaire = null;
    private Collection $champs;

    public function __construct()
    {
        $this->champs = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(?string $code): void
    {
        $this->code = $code;
    }

    public function getLibelle(): ?string
    {
        return $this->libelle;
    }

    public function setLibelle(?string $libelle): void
    {
        $this->libelle = $libelle;
    }

    public function getOrdre(): ?int
    {
        return $this->ordre;
    }

    public function setOrdre(?int $ordre): void
    {
        $this->ordre = $ordre;
    }

    public function getFormulaire(): ?Formulaire
    {
        return $this->formulaire;
    }

    public function setFormulaire(?Formulaire $formulaire): void
    {
        $this->formulaire = $formulaire;
    }

    /** @return Champ[] */
    public function getChamps(): array
    {
        return $this->champs->toArray();
    }

    public function addChamp(Champ $champ): void
    {
        $this->champs->add($champ);
    }

    public function removeChamp(Champ $champ): void
    {
        $this->champs->removeElement($champ);
    }

    public function hasChamp(Champ $champ): bool
    {
        return $this->champs->contains($champ);
    }

}