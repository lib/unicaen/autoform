<?php

namespace UnicaenAutoform\Entity\Db;

use UnicaenAutoform\Entity\HasMotsClefsAwareTrait;
use UnicaenAutoform\Entity\HasMotsClefsInterface;
use UnicaenUtilisateur\Entity\Db\HistoriqueAwareInterface;
use UnicaenUtilisateur\Entity\Db\HistoriqueAwareTrait;

class Champ implements HistoriqueAwareInterface, HasMotsClefsInterface {
    use HistoriqueAwareTrait;
    use HasMotsClefsAwareTrait;

    const TYPE_SPACER           = "Spacer"; //
    const TYPE_LABEL            = "Label"; //
    const TYPE_TEXT             = "Text"; //
    const TYPE_MULTIPLE_TEXT    = "Multiple Text"; //
    const TYPE_TEXTAREA         = "Textarea"; //
    const TYPE_CHECKBOX         = "Checkbox"; //
    const TYPE_SELECT           = "Select"; //
    const TYPE_SELECT_TEXT      = "Select_text"; //
    const TYPE_PERIODE          = "Periode"; // XXX
    const TYPE_FORMATION        = "Formation"; // XXX
    const TYPE_ANNEE            = "Annee"; // XXX
    const TYPE_NOMBRE           = "Number"; //
    const TYPE_MULTIPLE         = "Multiple"; //
    const TYPE_ENTITY           = "Entity"; //
    const TYPE_ENTITY_MULTI     = "Entity Multiple"; //
    const TYPE_CUSTOM           = "Multiple_champs_paramètrables"; //
    const TYPE_PLUS_TEXTE       = "Plus Text"; //
    const TYPE_PLUS_CUSTOM      = "Plus Custom"; //

    const SEPARATOR_PLUS_CUSTOM_GROUP = "|";
    const SEPARATOR_PLUS_CUSTOM_FIELD = ";";

    private ?int $id = null;
    private ?Categorie $categorie = null;
    private ?string $code = null;
    private ?string $libelle = null;
    private ?string $texte = null;
    private ?int $ordre = null;
    private ?ChampType $type = null;
    private ?string $balise = null;
    private ?string $options = null;
    private bool $obligatoire = false;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $id): void
    {
        $this->id = $id;
    }

    public function getCategorie() : ?Categorie
    {
        return $this->categorie;
    }

    public function setCategorie(?Categorie $categorie): void
    {
        $this->categorie = $categorie;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(?string $code): void
    {
        $this->code = $code;
    }

    public function getLibelle(): ?string
    {
        return $this->libelle;
    }

    public function setLibelle(?string $libelle): void
    {
        $this->libelle = $libelle;
    }

    public function getTexte(): ?string
    {
        return $this->texte;
    }

    public function setTexte(?string $texte): void
    {
        $this->texte = $texte;
    }

    public function getOrdre(): ?int
    {
        return $this->ordre;
    }

    public function setOrdre(?int $ordre): void
    {
        $this->ordre = $ordre;
    }

    public function getType(): ?ChampType
    {
        return $this->type;
    }

    public function setType(?ChampType $type): void
    {
        $this->type = $type;
    }



    /** todo (quid) What the fuck is it ??? */
    public function getBalise(): ?string
    {
        return $this->balise;
    }

    public function setBalise(?string $balise): void
    {
        $this->balise = $balise;
    }

    public function getOptions(): ?string
    {
        return $this->options;
    }

    public function setOptions(?string $options): void
    {
        $this->options = $options;
    }

    public function isObligatoire(): bool
    {
        return $this->obligatoire;
    }

    public function setObligatoire(bool $obligatoire): void
    {
        $this->obligatoire = $obligatoire;
    }

}