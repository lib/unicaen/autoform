<?php

namespace UnicaenAutoform\Entity;

interface HasMotsClefsInterface {

    public function getMotsClefs(): ?string;
    public function setMotsClefs(?string $motsclefs  = null): void;
    public function hasMotsClefs(array $mots) : bool;
}