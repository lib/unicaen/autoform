<?php

namespace UnicaenAutoform\Service\Categorie;

use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\QueryBuilder;
use DoctrineModule\Persistence\ProvidesObjectManager;
use Laminas\Mvc\Controller\AbstractActionController;
use RuntimeException;
use UnicaenAutoform\Entity\Db\Categorie;
use UnicaenAutoform\Entity\Db\Formulaire;
use UnicaenAutoform\Service\Champ\ChampServiceAwareTrait;

class CategorieService
{
    use ChampServiceAwareTrait;
    use ProvidesObjectManager;

    /** GESTION DES ENTITES *******************************************************************************************/

    public function create(Categorie $categorie): Categorie
    {
        $this->getObjectManager()->persist($categorie);
        $this->getObjectManager()->flush($categorie);
        return $categorie;
    }

    public function update(Categorie $categorie): Categorie
    {
        $this->getObjectManager()->flush($categorie);
        return $categorie;
    }

    public function historise(Categorie $categorie): Categorie
    {
        $categorie->historiser();
        $this->getObjectManager()->flush($categorie);
        return $categorie;
    }

    public function restore(Categorie $categorie): Categorie
    {
        $categorie->dehistoriser();
        $this->getObjectManager()->flush($categorie);
        return $categorie;
    }

    public function delete(Categorie $categorie): Categorie
    {
        $this->getObjectManager()->remove($categorie);
        $this->getObjectManager()->flush();
        return $categorie;
    }

    /** REQUETAGE *****************************************************************************************************/

    public function createQueryBuilder(): QueryBuilder
    {
        $qb = $this->getObjectManager()->getRepository(Categorie::class)->createQueryBuilder('categorie')
            ->leftJoin('categorie.formulaire', 'formulaire')
            ->leftJoin('categorie.champs', 'champ');
        return $qb;
    }

    public function getRequestedCategorie(AbstractActionController $controller, string $label = 'categorie'): ?Categorie
    {
        $id = $controller->params()->fromRoute($label);
        $categorie = $this->getCategorie($id);
        return $categorie;
    }

    /** @return Categorie[] */
    public function getCategories(string $champ = 'id', string $ordre = 'ASC'): array
    {
        $qb = $this->createQueryBuilder()
            ->orderBy('categorie.' . $champ, $ordre);

        $result = $qb->getQuery()->getResult();
        return $result;
    }

    /** @return Categorie[] */
    public function getCategoriesByFormulaire(Formulaire $formulaire, ?string $ordre = null): array
    {
        $qb = $this->createQueryBuilder()
            ->andWhere('categorie.formulaire = :formulaire')
            ->setParameter('formulaire', $formulaire);

        if ($ordre) $qb = $qb->orderBy('categorie.ordre', 'ASC');

        $result = $qb->getQuery()->getResult();
        return $result;
    }

    public function getCategorie(?int $id): ?Categorie
    {
        $qb = $this->createQueryBuilder()
            ->andWhere('categorie.id = :id')
            ->setParameter('id', $id);

        try {
            $result = $qb->getQuery()->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            throw new RuntimeException("Plusieurs Categorie partagent le même identifiant [" . $id . "].", $e);
        }
        return $result;
    }

    public function swapCategories(Categorie $categorie1, Categorie $categorie2): void
    {
        $buffer = $categorie1->getOrdre();
        $categorie1->setOrdre($categorie2->getOrdre());
        $categorie2->setOrdre($buffer);
        $this->update($categorie1);
        $this->update($categorie2);
    }

    public function getCategoriesAvecSens(Categorie $categorie, string $direction): array
    {
        $qb = $this->createQueryBuilder()
            ->andWhere('categorie.formulaire = :formulaire')
            ->setParameter('formulaire', $categorie->getFormulaire());

        $qb = match ($direction) {
            'haut' => $qb->andWhere('categorie.ordre < :position')
                ->setParameter('position', $categorie->getOrdre())
                ->orderBy('categorie.ordre', 'DESC'),
            'bas' => $qb->andWhere('categorie.ordre > :position')
                ->setParameter('position', $categorie->getOrdre())
                ->orderBy('categorie.ordre', 'ASC'),
            default => throw new RuntimeException("Direction non reconnue"),
        };

        $result = $qb->getQuery()->getResult();
        return $result;
    }

    public function compacter($categorie): void
    {
        $champs = $this->getChampService()->getChampsByCategorie($categorie, 'ordre');

        $position = 1;
        foreach ($champs as $champ) {
            $champ->setOrdre($position);
            $this->getChampService()->update($champ);
            $position++;
        }
    }

}