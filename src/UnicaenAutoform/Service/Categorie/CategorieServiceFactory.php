<?php

namespace UnicaenAutoform\Service\Categorie;

use Doctrine\ORM\EntityManager;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\ContainerInterface;
use Psr\Container\NotFoundExceptionInterface;
use UnicaenAutoform\Service\Champ\ChampService;

class CategorieServiceFactory
{

    /**
     * @param ContainerInterface $container
     * @return CategorieService
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container): CategorieService
    {
        /**
         * @var EntityManager $entityManager
         * @var ChampService $champService
         */
        $entityManager = $container->get('doctrine.entitymanager.orm_default');
        $champService = $container->get(ChampService::class);

        $service = new CategorieService();
        $service->setObjectManager($entityManager);
        $service->setChampService($champService);
        return $service;
    }
}