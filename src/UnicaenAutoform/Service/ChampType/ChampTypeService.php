<?php

namespace UnicaenAutoform\Service\ChampType;

use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\QueryBuilder;
use DoctrineModule\Persistence\ProvidesObjectManager;
use Laminas\Mvc\Controller\AbstractActionController;
use RuntimeException;
use UnicaenAutoform\Entity\Db\ChampType;

class ChampTypeService {
    use ProvidesObjectManager;

    /** GESTION DES ENTITÉS *******************************************************************************************/

    public function create(ChampType $type): ChampType
    {
        $this->getObjectManager()->persist($type);
        $this->getObjectManager()->flush();
        return $type;
    }

    public function update(ChampType $type): ChampType
    {
        $this->getObjectManager()->persist($type);
        $this->getObjectManager()->flush();
        return $type;
    }

    public function delete(ChampType $type): ChampType
    {
        $this->getObjectManager()->remove($type);
        $this->getObjectManager()->flush();
        return $type;
    }

    /** QUERRYING *****************************************************************************************************/

    public function createQueryBuilder(bool $joinOnChamp = false) : QueryBuilder
    {
        $qb = $this->getObjectManager()->getRepository(ChampType::class)->createQueryBuilder('champtype');
        if ($joinOnChamp) $qb = $qb->leftJoin("champtype.champs", "champ")->addSelect("champ");

        return $qb;
    }

    public function getChampType(string $code, bool $joinOnChamp = false): ?ChampType
    {
        $qb = $this->createQueryBuilder($joinOnChamp)
            ->where('champtype.code = :code')->setParameter('code', $code)
        ;
        try {
            $result = $qb->getQuery()->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            throw new RuntimeException("Plusieurs [".ChampType::class."] partagent le même code [".$code."]", 0 , $e);
        }
        return $result;
    }

    public function getRequestedChampType(AbstractActionController $controller, string $params='champ-type', bool $joinOnChamp=false): ?ChampType
    {
        $id = $controller->params()->fromRoute($params);
        $result = $this->getChampType($id, $joinOnChamp);
        return $result;
    }

    /** @return ChampType[] */
    public function getChampTypes(string $champ = 'libelle', string $ordre='ASC', bool $joinOnChamp=false): array
    {
        $qb = $this->createQueryBuilder($joinOnChamp)
            ->orderBy('champtype.'.$champ, $ordre);
        if ($joinOnChamp) $qb = $qb->leftJoin("champtype.champs", "champ")->addSelect("champ");

        $result = $qb->getQuery()->getResult();
        return $result;
    }

    /** FACADE ********************************************************************************************************/

    public function getChampTypesAsOptions(): array {
        $types = $this->getChampTypes();

        $options = [];
        foreach ($types as $type) {
            $options[$type->getCode()] = $type->getLibelle();
        }
        return $options;
    }
}