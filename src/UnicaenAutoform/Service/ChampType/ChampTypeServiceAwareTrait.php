<?php

namespace UnicaenAutoform\Service\ChampType;

trait ChampTypeServiceAwareTrait {

    private ChampTypeService $champTypeService;

    public function getChampTypeService(): ChampTypeService
    {
        return $this->champTypeService;
    }

    public function setChampTypeService(ChampTypeService $champTypeService): void
    {
        $this->champTypeService = $champTypeService;
    }

}