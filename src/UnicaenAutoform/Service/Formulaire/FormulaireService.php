<?php

namespace UnicaenAutoform\Service\Formulaire;

use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\QueryBuilder;
use DoctrineModule\Persistence\ProvidesObjectManager;
use Laminas\Mvc\Controller\AbstractActionController;
use RuntimeException;
use UnicaenAutoform\Entity\Db\Champ;
use UnicaenAutoform\Entity\Db\Formulaire;
use UnicaenAutoform\Service\Categorie\CategorieServiceAwareTrait;

class FormulaireService
{
    use ProvidesObjectManager;
    use CategorieServiceAwareTrait;

    /** GESTION DES ENTITES *******************************************************************************************/

    public function create(Formulaire $formulaire): Formulaire
    {
        $this->getObjectManager()->persist($formulaire);
        $this->getObjectManager()->flush($formulaire);
        return $formulaire;
    }

    public function update(Formulaire $formulaire): Formulaire
    {
        $this->getObjectManager()->flush($formulaire);
        return $formulaire;
    }

    public function historise(Formulaire $formulaire): Formulaire
    {
        $formulaire->historiser();
        $this->getObjectManager()->flush($formulaire);
        return $formulaire;
    }

    public function restore(Formulaire $formulaire): Formulaire
    {
        $formulaire->dehistoriser();
        $this->getObjectManager()->flush($formulaire);
        return $formulaire;
    }

    public function delete(Formulaire $formulaire): Formulaire
    {
        $this->getObjectManager()->remove($formulaire);
        $this->getObjectManager()->flush();
        return $formulaire;
    }

    /** REQUETAGES ****************************************************************************************************/

    /**
     * @return QueryBuilder
     */
    public function createQueryBuilder(): QueryBuilder
    {
        $qb = $this->getObjectManager()->getRepository(Formulaire::class)->createQueryBuilder('formulaire')
            ->addSelect('categorie')->leftJoin('formulaire.categories', 'categorie')
            ->addSelect('champ')->leftJoin('categorie.champs', 'champ');
        return $qb;
    }

    /**
     * @return Formulaire[]
     */
    public function getFormulaires(): array
    {
        $qb = $this->createQueryBuilder();
        $result = $qb->getQuery()->getResult();

        $array = [];
        /** @var Formulaire $formulaire */
        foreach ($result as $formulaire) {
            $array[$formulaire->getCode()] = $formulaire;
        }
        return $array;
    }

    /**
     * @param int|null $id
     * @return Formulaire|null
     */
    public function getFormulaire(?int $id): ?Formulaire
    {
        $qb = $this->createQueryBuilder()
            ->andWhere('formulaire.id = :id')
            ->setParameter('id', $id);

        try {
            $result = $qb->getQuery()->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            throw new RuntimeException("Plusieurs Formulaire partagent le même identifiant [" . $id . "].", $e);
        }
        return $result;
    }

    public function getFormulaireByCode(?string $code): ?Formulaire
    {
        $qb = $this->createQueryBuilder()
            ->andWhere('formulaire.code = :code')
            ->setParameter('code', $code);

        try {
            $result = $qb->getQuery()->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            throw new RuntimeException("Plusieurs Formulaire partagent le même code [" . $code . "].", $e);
        }
        return $result;
    }

    public function getRequestedFormulaire(AbstractActionController $controller, string $label = 'formulaire'): ?Formulaire
    {
        $id = $controller->params()->fromRoute($label);
        $formulaire = $this->getFormulaire($id);
        return $formulaire;
    }

    public function compacter(Formulaire $formulaire): void
    {
        $categories = $this->getCategorieService()->getCategoriesByFormulaire($formulaire, 'ordre');

        $position = 1;
        foreach ($categories as $categorie) {
            $categorie->setOrdre($position);
            $this->getCategorieService()->update($categorie);
            $position++;
        }
    }

    /** @return Champ[] */
    public function getChampsAsOptions(Formulaire $formulaire): array
    {
        $champs = [];
        foreach ($formulaire->getCategories() as $categorie) {
            foreach ($categorie->getChamps() as $champ) {
                if ($champ->getType()->getCode() !== 'Spacer' and $champ->getType()->getCode() !== 'Label')
                    $champs[] = $champ;
            }
        }

        usort($champs, function (Champ $a, Champ $b) {
            $alib = $a->getCategorie()->getLibelle() . " > " . $a->getLibelle();
            $blib = $b->getCategorie()->getLibelle() . " > " . $b->getLibelle();
            return $alib <=> $blib;
        });

        $array = [];
        foreach ($champs as $champ) {
            $array[$champ->getId()] = $champ->getCategorie()->getLibelle() . " > " . $champ->getLibelle();
        }

        return $array;
    }
}
