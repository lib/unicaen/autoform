<?php

namespace UnicaenAutoform\Service\Formulaire;

use Doctrine\ORM\EntityManager;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\ContainerInterface;
use Psr\Container\NotFoundExceptionInterface;
use UnicaenUtilisateur\Service\User\UserService;

class FormulaireInstanceServiceFactory {

    /**
     * @param ContainerInterface $container
     * @return FormulaireInstanceService
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container): FormulaireInstanceService
    {
        /**
         * @var EntityManager $entityManager
         * @var FormulaireService $formulaireService
         * @var FormulaireReponseService $formulaireReponseService
         */
        $entityManager = $container->get('doctrine.entitymanager.orm_default');
        $formulaireService = $container->get(FormulaireService::class);
        $formulaireReponseService = $container->get(FormulaireReponseService::class);
        $userService = $container->get(UserService::class);

        $service = new FormulaireInstanceService();
        $service->setObjectManager($entityManager);
        $service->setFormulaireService($formulaireService);
        $service->setFormulaireReponseService($formulaireReponseService);
        $service->setUserService($userService);
        return $service;
    }
}