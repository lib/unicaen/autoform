<?php

namespace UnicaenAutoform\Service\Formulaire;

trait FormulaireReponseServiceAwareTrait {

    private FormulaireReponseService $formulaireReponseService;

    public function getFormulaireReponseService(): FormulaireReponseService
    {
        return $this->formulaireReponseService;
    }

    public function setFormulaireReponseService(FormulaireReponseService $formulaireReponseService): void
    {
        $this->formulaireReponseService = $formulaireReponseService;
    }


}