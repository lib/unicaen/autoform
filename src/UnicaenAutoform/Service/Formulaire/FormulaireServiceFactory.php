<?php

namespace UnicaenAutoform\Service\Formulaire;

use Doctrine\ORM\EntityManager;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\ContainerInterface;
use Psr\Container\NotFoundExceptionInterface;
use UnicaenAutoform\Service\Categorie\CategorieService;

class FormulaireServiceFactory
{

    /**
     * @param ContainerInterface $container
     * @return FormulaireService
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container): FormulaireService
    {
        /**
         * @var EntityManager $entityManager
         * @var CategorieService $categorieService
         */
        $entityManager = $container->get('doctrine.entitymanager.orm_default');
        $categorieService = $container->get(CategorieService::class);

        $service = new FormulaireService();
        $service->setObjectManager($entityManager);
        $service->setCategorieService($categorieService);
        return $service;
    }
}