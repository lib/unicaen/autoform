<?php

namespace UnicaenAutoform\Service\Formulaire;

trait FormulaireInstanceServiceAwareTrait {

    private FormulaireInstanceService $formulaireInstanceService;

    public function getFormulaireInstanceService(): FormulaireInstanceService
    {
        return $this->formulaireInstanceService;
    }

    public function setFormulaireInstanceService(FormulaireInstanceService $formulaireInstanceService): void
    {
        $this->formulaireInstanceService = $formulaireInstanceService;
    }


}