<?php

namespace UnicaenAutoform\Service\Formulaire;

use DateTime;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\QueryBuilder;
use DoctrineModule\Persistence\ProvidesObjectManager;
use Laminas\Mvc\Controller\AbstractActionController;
use RuntimeException;
use UnicaenAutoform\Entity\Db\FormulaireInstance;
use UnicaenAutoform\Entity\Db\FormulaireReponse;
use UnicaenUtilisateur\Entity\Db\UserInterface;
use UnicaenUtilisateur\Service\User\UserServiceAwareTrait;

class FormulaireInstanceService
{
    use FormulaireServiceAwareTrait;
    use FormulaireReponseServiceAwareTrait;
    use ProvidesObjectManager;
    use UserServiceAwareTrait;

    /** GESTION DES ENTITES *******************************************************************************************/

    /**
     * @param FormulaireInstance $instance
     * @return FormulaireInstance
     */
    public function create(FormulaireInstance $instance): FormulaireInstance
    {
        $this->getObjectManager()->persist($instance);
        $this->getObjectManager()->flush($instance);
        return $instance;
    }

    /**
     * @param FormulaireInstance $instance
     * @return FormulaireInstance
     */
    public function update(FormulaireInstance $instance): FormulaireInstance
    {
        $this->getObjectManager()->flush($instance);
        return $instance;
    }

    /**
     * @param FormulaireInstance $instance
     * @return FormulaireInstance
     */
    public function historise(FormulaireInstance $instance): FormulaireInstance
    {
        $instance->historiser();
        $this->getObjectManager()->flush($instance);
        return $instance;
    }

    /**
     * @param FormulaireInstance $instance
     * @return FormulaireInstance
     */
    public function restore(FormulaireInstance $instance): FormulaireInstance
    {
        $instance->dehistoriser();
        $this->getObjectManager()->flush($instance);
        return $instance;
    }

    /**
     * @param FormulaireInstance $instance
     * @return FormulaireInstance
     */
    public function delete(FormulaireInstance $instance): FormulaireInstance
    {
        $this->getObjectManager()->remove($instance);
        $this->getObjectManager()->flush($instance);
        return $instance;
    }

    /** REQUETAGES ****************************************************************************************************/

    public function createQueryBuilder(): QueryBuilder
    {
        $qb = $this->getObjectManager()->getRepository(FormulaireInstance::class)->createQueryBuilder('formulaire_instance')
            ->addSelect('formulaire')->join('formulaire_instance.formulaire', 'formulaire')
            ->addSelect('categorie')->join('formulaire.categories', 'categorie')
            ->addSelect('champ')->join('categorie.champs', 'champ')
            ->addSelect('reponse')->leftJoin('formulaire_instance.reponses', 'reponse');
        return $qb;
    }

    public function getFormulaireInstance(?int $id): ?FormulaireInstance
    {
        $qb = $this->createQueryBuilder()
            ->andWhere('formulaire_instance.id = :id')
            ->setParameter('id', $id);

        try {
            $result = $qb->getQuery()->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            throw new RuntimeException("Plusieurs FormulaireInstance partagent le même identifiant [" . $id . "]", $e);
        }
        return $result;
    }

    public function getRequestedFormulaireInstance(AbstractActionController $controller, string $label = 'instance'): ?FormulaireInstance
    {
        $id = $controller->params()->fromRoute($label);
        $instance = $this->getFormulaireInstance($id);
        return $instance;
    }

    /** CREATION ET DUPLICATION ... ***********************************************************************************/

    public function duplicate(FormulaireInstance $reference, FormulaireInstance $instance, ?UserInterface $user = null): void
    {
        if ($user === null) $user = $this->getUserService()->getConnectedUser();

        $dictionnaire = [];
        foreach ($instance->getReponses() as $reponseInstance) {
            if ($reponseInstance->estNonHistorise()) $dictionnaire[$reponseInstance->getChamp()->getId()][] = $reponseInstance;
        }

        foreach ($reference->getReponses() as $reponseReference) {

            $champId = $reponseReference->getChamp()->getId();

            // Si reponse dans le dictionnaire alors historiser pour recréer la nouvelle sans clash
            if (isset($dictionnaire[$champId])) {
                foreach ($dictionnaire[$champId] as $r) {
                    $r->setHistoDestructeur($user);
                    $r->setHistoDestruction(new DateTime());
                    $this->getFormulaireReponseService()->update($r);
                }
            }

            // Recopie
            $newReponse = new FormulaireReponse();
            $newReponse->setFormulaireInstance($instance);
            $newReponse->setChamp($reponseReference->getChamp());
            $newReponse->setReponse($reponseReference->getReponse());
            $newReponse->setHistoCreateur($user);
            $newReponse->setHistoModificateur($user);
            $this->getFormulaireReponseService()->create($newReponse);
            $instance->addReponse($newReponse);
        }
        $this->update($instance);
    }

    public function recopie(FormulaireInstance $instance1, FormulaireInstance $instance2, int $champId1, int $champId2): void
    {
        $reponses = $instance1->getReponses();
        foreach ($reponses as $reponse) {
            if ($reponse->getChamp()->getId() == $champId1) {
                $champ = $instance2->getChamp($champId2);
                if ($champ !== null) {
                    $value = $reponse->getReponse();
                    $new = new FormulaireReponse();
                    $new->setFormulaireInstance($instance2);
                    $new->setChamp($champ);
                    $new->setReponse($value);
                    $this->getFormulaireReponseService()->create($new);
                }
            }
        }
    }

    public function createInstance(string $code): FormulaireInstance
    {
        $instance = new FormulaireInstance();
        $formulaire = $this->getFormulaireService()->getFormulaireByCode($code);
        if ($formulaire === null) {
            throw new RuntimeException("Aucun formulaire n'a pu être trouvé avec le code [" . $code . "]");
        }
        $instance->setFormulaire($formulaire);
        $this->create($instance);
        return $instance;
    }
}
