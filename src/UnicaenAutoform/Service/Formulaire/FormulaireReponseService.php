<?php

namespace UnicaenAutoform\Service\Formulaire;

use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\QueryBuilder;
use DoctrineModule\Persistence\ProvidesObjectManager;
use UnicaenApp\Exception\RuntimeException;
use UnicaenAutoform\Entity\Db\Champ;
use UnicaenAutoform\Entity\Db\Formulaire;
use UnicaenAutoform\Entity\Db\FormulaireInstance;
use UnicaenAutoform\Entity\Db\FormulaireReponse;
use UnicaenAutoform\Service\Champ\ChampServiceAwareTrait;

class FormulaireReponseService
{
    use ProvidesObjectManager;
    use ChampServiceAwareTrait;

    /** GESTION DES ENTITES  ******************************************************************************************/

    public function create(FormulaireReponse $reponse): FormulaireReponse
    {
        $this->getObjectManager()->persist($reponse);
        $this->getObjectManager()->flush($reponse);
        return $reponse;
    }

    public function update(FormulaireReponse $reponse): FormulaireReponse
    {
        $this->getObjectManager()->flush($reponse);
        return $reponse;
    }

    public function historise(FormulaireReponse $reponse): FormulaireReponse
    {
        $reponse->historiser();
        $this->getObjectManager()->flush($reponse);
        return $reponse;
    }


    public function restore(FormulaireReponse $reponse): FormulaireReponse
    {
        $reponse->dehistoriser();
        $this->getObjectManager()->flush($reponse);
        return $reponse;
    }

    public function delete(FormulaireReponse $reponse): FormulaireReponse
    {
        $this->getObjectManager()->remove($reponse);
        $this->getObjectManager()->flush();
        return $reponse;
    }

    /** REQUETAGE *****************************************************************************************************/

    public function createQueryBuilder(): QueryBuilder
    {
        $qb = $this->getObjectManager()->getRepository(FormulaireReponse::class)->createQueryBuilder('reponse')
            ->leftJoin('reponse.instance', 'finstance')
            ->leftJoin('reponse.champ', 'champ');

        return $qb;
    }

    /** @return FormulaireReponse[] */
    public function getFormulaireReponses(): array
    {
        $qb = $this->createQueryBuilder();
        $result = $qb->getQuery()->getResult();

        return $result;
    }




    public function getFormulaireReponse(?int $id): ?FormulaireReponse
    {
        $qb = $this->createQueryBuilder()
            ->andWhere('reponse.id = :id')
            ->setParameter('id', $id);

        try {
            $result = $qb->getQuery()->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            throw new RuntimeException("Plusieurs Reponse partagent le même identifiant [" . $id . "].", $e);
        }

        return $result;
    }


    public function updateFormulaireReponse(Formulaire $formulaire, FormulaireInstance $instance, $data): void
    {
        /** @var Champ[] $champs */
        $qb = $this->getObjectManager()->getRepository(Champ::class)->createQueryBuilder('champ')
            ->addSelect('categorie')->join('champ.categorie', 'categorie')
            ->andWhere('categorie.formulaire = :formulaire')
            ->andWhere('categorie.histoDestruction IS NULL')
            ->andWhere('champ.histoDestruction IS NULL')
            ->setParameter('formulaire', $formulaire);
        $champs = $qb->getQuery()->getResult();

        $reponses = $this->getFormulaireResponsesByFormulaireInstance($instance);

        foreach ($champs as $champ) {
            $value = $this->getValueFomData($champ, $data);
            if ($value !== null && $value !== "") {
                $found = null;
                foreach ($reponses as $reponse) {
                    if ($reponse->getChamp()->getId() === $champ->getId()) {
                        $found = $reponse;
                        break;
                    }
                }
                if ($found !== null) {
                    // Le champ existe et a une réponse :: mise à jour
                    // TODO :: historise duplique avec la nouvelle valeur
                    $reponse = $found;
                    $reponse->setReponse($value);
                    $this->update($reponse);
                } else {
                    // Le champ n'existe pas et une réponse doit être sauvegardé : creation
                    $reponse = new FormulaireReponse();
                    $reponse->setFormulaireInstance($instance);
                    $reponse->setChamp($champ);
                    $reponse->setReponse($value);
                    $this->create($reponse);
                }
            } else {
                foreach ($reponses as $reponse) {
                    // Le champ existe mais n'a plus de réponse :: destruction
                    // TODO :: devrait être historisée
                    if ($reponse->getChamp()->getId() === $champ->getId()) {
                        $this->delete($reponse); //cas A
                        break;
                    }
                }
            }
        }
    }


    /** @return FormulaireReponse[] */
    public function getFormulaireResponsesByFormulaireInstance(FormulaireInstance $instance): array
    {
        $qb = $this->createQueryBuilder()
            ->andWhere('reponse.instance = :instance')
            ->andWhere('reponse.histoDestruction IS NULL')
            ->setParameter('instance', $instance);

        $result = $qb->getQuery()->getResult();

        $reponses = [];
        /** @var FormulaireReponse $item */
        foreach ($result as $item) {
            $reponses[$item->getChamp()->getId()] = $item;
        }

        return $reponses;
    }


    public function getValueFomData(Champ $champ, $data): ?string
    {
        switch ($champ->getType()->getCode()) {
            case Champ::TYPE_CHECKBOX :
                return $data[$champ->getId()] ?? null;
            case Champ::TYPE_TEXT :
            case Champ::TYPE_NOMBRE :
            case Champ::TYPE_TEXTAREA :
                if (isset($data[$champ->getId()])) {
                    $value = trim($data[$champ->getId()]);
                    $value = strip_tags($value, ['p', 'strong', 'em', 'ol', 'ul', 'li']);
                    return ($value !== '') ? $value : null;
                } else return null;
            case Champ::TYPE_SELECT_TEXT :
                if (isset($data[$champ->getId()])) {
                    $value = $data[$champ->getId()];
                    if ($value != null && str_ends_with($data[$champ->getId()], '*') && isset($data['text_complementaire_' . $champ->getId()])) {
                        return $data[$champ->getId()] . ":::" . $data['text_complementaire_' . $champ->getId()];
                    } else {
                        return ($value !== 'null') ? $value : null;
                    }
                } else return null;
            case Champ::TYPE_SELECT :
            case Champ::TYPE_ENTITY :
            case Champ::TYPE_ANNEE :
                if (isset($data[$champ->getId()])) {
                    $value = $data[$champ->getId()];

                    return ($value !== 'null') ? $value : null;
                } else return null;
            case Champ::TYPE_ENTITY_MULTI:
                $instances = $this->getChampService()->getAllInstance($champ->getOptions());
                $values = [];
                foreach ($instances as $id => $instance) {
                    if (isset($data[$champ->getId() . "_" . $id])) {
                        $values[] = $id;
                    }
                }

                return implode(";", $values);
            case Champ::TYPE_MULTIPLE :
                $options = explode(";", $champ->getOptions());
                $values = [];
                foreach ($options as $option) {
                    $option = str_replace(" ", "_", $option);
                    if (isset($data[$champ->getId() . "_" . $option])) {
                        $values[] = "on_" . $option;
                    }
                }

                return implode(";", $values);
            case Champ::TYPE_MULTIPLE_TEXT :
                $options = explode(";", $champ->getOptions());
                $values = [];
                $hasData = false;
                for ($position = 0; $position < count($options); $position++) {
                    $tmp = "";
                    if (isset($data[$champ->getId() . "_" . $position]) and trim($data[$champ->getId() . "_" . $position]) !== "") {
                        $tmp = $data[$champ->getId() . "_" . $position];
                        $hasData = true;
                    }
                    $values[] = $tmp;
                }

                return ($hasData) ? implode(";", $values) : null;
            case Champ::TYPE_PERIODE :
                $select = $data['select_' . $champ->getId()];
                if ($select === 'null') return null;
                if ($select !== 'Balisée') {
                    return $select;
                } else {
                    if ($data['debut_' . $champ->getId()] === '' || $data['fin_' . $champ->getId()] === '') return null;
                    if ($data['debut_' . $champ->getId()] > $data['fin_' . $champ->getId()]) return null;
                    $row_date1 = $data['debut_' . $champ->getId()];
                    $date1 = implode("/", array_reverse(explode('-', $row_date1)));
                    $row_date2 = $data['fin_' . $champ->getId()];
                    $date2 = implode("/", array_reverse(explode('-', $row_date2)));

                    return 'Du ' . $date1 . ' au ' . $date2;
                }
            case Champ::TYPE_FORMATION :
                $text = trim($data['text_' . $champ->getId()]);
                $select = trim($data['select_' . $champ->getId()]);
                if ($text !== "") {
                    return $text . "|" . $select;
                }

                return null;
            case Champ::TYPE_CUSTOM :
                $nbOptions = count(explode(";", $champ->getOptions()));
                $hasData = false;
                $values = [];
                for ($position = 0; $position < $nbOptions; $position++) {
                    $tmp = "";
                    if (isset($data[$champ->getId() . "_" . $position]) and trim($data[$champ->getId() . "_" . $position]) !== "") {
                        $tmp = trim($data[$champ->getId() . "_" . $position]);
                        $hasData = true;
                    }
                    $values[] = $tmp;
                }

                return ($hasData) ? implode(";", $values) : null;
            case Champ::TYPE_PLUS_TEXTE :
                $listing = [];
                $prefix = "input_" . $champ->getId() . "_";
                foreach ($data as $id => $value) {
                    if (str_starts_with($id, $prefix)) {
                        $listing[] = $value;
                    }
                }
                $listing = array_filter($listing, function ($texte) {
                    return $texte !== '';
                });
                $texte = implode(";", $listing);
                return $texte;
            case Champ::TYPE_PLUS_CUSTOM :
                $nbFields = count(explode(";", $champ->getOptions()));

                $relatedData = [];
                foreach ($data as $id => $value) {
                    if (str_starts_with($id, 'input_' . $champ->getId())) {
                        $relatedData[$id] = $value;
                    }
                }

                $dictionary = [];
                foreach ($relatedData as $id => $value) {
                    [$input, $id, $groupe, $position] = explode("_", $id);
                    $dictionary[$groupe][$position] = $value;
                }

                if (empty($dictionary)) return null;

                $concat = [];
                foreach ($dictionary as $items) {
                    $subconcat = [];
                    for($i = 0 ; $i <$nbFields ; $i++) {
                        $subconcat[] = $items[$i]??"";
                    }
                    $concat[] = implode(Champ::SEPARATOR_PLUS_CUSTOM_FIELD, $subconcat);
                }
                $values =  implode(Champ::SEPARATOR_PLUS_CUSTOM_GROUP, $concat);
                return $values;

            default:
                return null;
        }
    }





}