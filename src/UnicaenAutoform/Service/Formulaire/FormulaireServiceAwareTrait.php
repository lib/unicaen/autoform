<?php

namespace UnicaenAutoform\Service\Formulaire;

trait FormulaireServiceAwareTrait
{

    private FormulaireService $formulaireService;

    public function getFormulaireService(): FormulaireService
    {
        return $this->formulaireService;
    }

    public function setFormulaireService(FormulaireService $formulaireService): FormulaireService
    {
        $this->formulaireService = $formulaireService;
        return $this->formulaireService;
    }

}