<?php

namespace UnicaenAutoform\Service\Formulaire;

use Psr\Container\ContainerExceptionInterface;
use Psr\Container\ContainerInterface;
use Psr\Container\NotFoundExceptionInterface;
use UnicaenAutoform\Service\Champ\ChampService;
use Doctrine\ORM\EntityManager;

class FormulaireReponseServiceFactory {

    /**
     * @param ContainerInterface $container
     * @return FormulaireReponseService
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container): FormulaireReponseService
    {
        /**
         * @var EntityManager $entityManager
         * @var ChampService $champService
         */
        $entityManager = $container->get('doctrine.entitymanager.orm_default');
        $champService = $container->get(ChampService::class);

        $service = new FormulaireReponseService();
        $service->setObjectManager($entityManager);
        $service->setChampService($champService);
        return $service;
    }
}