<?php

namespace UnicaenAutoform\Service\Champ;

trait ChampServiceAwareTrait {

    private ChampService $champService;

    public function getChampService(): ChampService
    {
        return $this->champService;
    }

    public function setChampService(ChampService $champService): void
    {
        $this->champService = $champService;
    }


}