<?php

namespace UnicaenAutoform\Service\Champ;

use Doctrine\ORM\EntityManager;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\ContainerInterface;
use Psr\Container\NotFoundExceptionInterface;

class ChampServiceFactory {

    /**
     * @param ContainerInterface $container
     * @return ChampService
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container): ChampService
    {
        /**
         * @var EntityManager $entityManager
         */
        $entityManager = $container->get('doctrine.entitymanager.orm_default');

        $service = new ChampService();
        $service->setObjectManager($entityManager);
        return $service;
    }
}