<?php

namespace UnicaenAutoform\Service\Champ;

use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\QueryBuilder;
use DoctrineModule\Persistence\ProvidesObjectManager;
use Laminas\Mvc\Controller\AbstractActionController;
use RuntimeException;
use UnicaenAutoform\Entity\Db\Categorie;
use UnicaenAutoform\Entity\Db\Champ;

class ChampService
{
    use ProvidesObjectManager;

    /** GESTION DES ENTITES *******************************************************************************************/

    public function create(Champ $champ): Champ
    {
        $this->getObjectManager()->persist($champ);
        $this->getObjectManager()->flush($champ);
        return $champ;
    }

    public function update(Champ $champ): Champ
    {
        $this->getObjectManager()->flush($champ);
        return $champ;
    }

    public function historise(Champ $champ): Champ
    {
        $champ->historiser();
        $this->getObjectManager()->flush($champ);
        return $champ;
    }

    public function restore(Champ $champ): Champ
    {
        $champ->dehistoriser();
        $this->getObjectManager()->flush($champ);
        return $champ;
    }

    /**
     * @param Champ $champ
     * @return Champ
     */
    public function delete(Champ $champ): Champ
    {
        $this->getObjectManager()->remove($champ);
        $this->getObjectManager()->flush();
        return $champ;
    }

    /** REQUETAGE *****************************************************************************************************/

    public function createQueryBuilder(): QueryBuilder
    {
        $qb = $this->getObjectManager()->getRepository(Champ::class)->createQueryBuilder('champ')
            ->leftJoin('champ.categorie', 'categorie');
        return $qb;
    }

    public function getRequestedChamp(AbstractActionController $controller, string $label = 'champ'): ?Champ
    {
        $id = $controller->params()->fromRoute($label);
        $champ = $this->getChamp($id);
        return $champ;
    }

    /** @return Champ[] */
    public function getChamps(string $champ = 'id', string $order = 'ASC'): array
    {
        $qb = $this->createQueryBuilder()
            ->orderBy('champ.' . $champ, $order);

        $result = $qb->getQuery()->getResult();
        return $result;
    }

    /** @return Champ[] */
    public function getChampsByCategorie(Categorie $categorie, string $ordre = null): array
    {
        $qb = $this->createQueryBuilder()
            ->andWhere('champ.categorie = :categorie')
            ->setParameter('categorie', $categorie);

        if ($ordre) $qb = $qb->orderBy('champ.ordre', 'ASC');

        $result = $qb->getQuery()->getResult();
        return $result;
    }


    public function getChamp(?int $id): ?Champ
    {
        $qb = $this->createQueryBuilder()
            ->andWhere('champ.id = :id')
            ->setParameter('id', $id);

        try {
            $result = $qb->getQuery()->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            throw new RuntimeException("Plusieurs Champ partagent le même identifiant [" . $id . "].", $e);
        }
        return $result;
    }

    public function swapChamps(Champ $champ1, Champ $champ2): void
    {
        $buffer = $champ1->getOrdre();
        $champ1->setOrdre($champ2->getOrdre());
        $champ2->setOrdre($buffer);
        $this->update($champ1);
        $this->update($champ2);
    }

    /** @return Champ[] */
    public function getChampsAvecSens(Champ $champ, string $direction): array
    {
        $qb = $this->createQueryBuilder()
            ->andWhere('champ.categorie = :categorie')
            ->setParameter('categorie', $champ->getCategorie());

        $qb = match ($direction) {
            'haut' => $qb->andWhere('champ.ordre < :position')
                ->setParameter('position', $champ->getOrdre())
                ->orderBy('champ.ordre', 'DESC'),
            'bas' => $qb->andWhere('champ.ordre > :position')
                ->setParameter('position', $champ->getOrdre())
                ->orderBy('champ.ordre', 'ASC'),
            default => throw new RuntimeException("Direction non reconnue"),
        };

        $result = $qb->getQuery()->getResult();
        return $result;
    }


    public function getAllInstance($entity): array
    {
        $instances = $this->getObjectManager()->getRepository($entity)->findAll();
        usort($instances, function ($a, $b) {
            return $a->getLibelle() <=> $b->getLibelle();
        });
        $array = [];
        foreach ($instances as $instance) {
            $array[$instance->getId()] = $instance->getLibelle();
        }
        return $array;
    }


}