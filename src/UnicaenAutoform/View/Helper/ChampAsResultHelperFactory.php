<?php

namespace UnicaenAutoform\View\Helper;

use Psr\Container\ContainerExceptionInterface;
use Psr\Container\ContainerInterface;
use Psr\Container\NotFoundExceptionInterface;
use UnicaenAutoform\Service\Champ\ChampService;

class ChampAsResultHelperFactory {

    /**
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container): ChampAsResultHelper
    {

        /**
         * @var ChampService $champService
         */
        $champService = $container->get(ChampService::class);

        $helper = new ChampAsResultHelper();
        $helper->setChampService($champService);
        return $helper;
    }
}