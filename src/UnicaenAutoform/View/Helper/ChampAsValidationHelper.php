<?php

namespace UnicaenAutoform\View\Helper;

use UnicaenAutoform\Entity\Db\Champ;
use UnicaenAutoform\Service\Champ\ChampServiceAwareTrait;
use Laminas\Form\View\Helper\AbstractHelper;

class ChampAsValidationHelper extends AbstractHelper
{
    use ChampServiceAwareTrait;

    /**
     * @param Champ $champ
     * @param array $data
     * @param boolean $validation
     * @return string
     */
    public function render($champ, $data = null, $validation = null) {
        $texte = "";

        switch($champ->getType()->getCode()) {
            case Champ::TYPE_LABEL : return "";
                break;
            case Champ::TYPE_SPACER : return "";
                break;
            case Champ::TYPE_MULTIPLE :
                if ($data !== 'null') {
                    $splits = explode(";", $data);
                    $validation_splits = ($validation !== null)?explode(";",$validation):[];

                    $texte .= '<div class="col-sm-offset-1 col-sm-11">';
                    $texte .= $champ->getLibelle(). "&nbsp;:";
                    $texte .= "<ul style='list-style: none;'>";
                    foreach ($splits as $split) {
                        $replace = str_replace("_"," ",substr($split,3));
                        $found = array_search($split, $validation_splits);
//                        $found = array_search($split, $data);
                        $texte .= "<li>";
                        $texte .= '<input type="checkbox" ';
                        $texte .= '     name="reponse_'.$champ->getId().'_'.$split.'" ';
                        $texte .= '       id="reponse_'.$champ->getId().'_'.$split.'" ';
                        $texte .=       (($found !== false)?'checked':'');
                        $texte .= ' >';
                        $texte .= '<label for="reponse_'.$champ->getId().'_'.$split.'"">&nbsp;'. $replace . '</label>';
                        $texte .= "</li>";
//                        $texte .= "<li>" . $split . "</li>";
                    }
                    $texte .= "</ul>";
                    $texte .= '</div>';
                    return $texte;
                }
                break;
            case Champ::TYPE_ENTITY:
                $options = $this->getChampService()->getAllInstance($champ->getOptions());
                $reponse = "NOT FOUND !!!";
                foreach ($options as $id => $option) {
                    if ($id == $data) {
                        $reponse = $option;
                        break;
                    }
                }
                $texte .= $champ->getLibelle(). "&nbsp;:&nbsp;".$reponse;
                break;
            case Champ::TYPE_CHECKBOX :
                if ($data === null OR $data === 'on') $texte .= ($champ->getTexte())?:$champ->getLibelle();
                break;
            case Champ::TYPE_TEXT :
            case Champ::TYPE_NOMBRE :
            case Champ::TYPE_TEXTAREA :
                $texte .= $champ->getLibelle(). ' : ';
                if ($data !== '') {
                    $texte .= $data;
                }
                break;
            case Champ::TYPE_SELECT_TEXT :
            case Champ::TYPE_SELECT :
            case Champ::TYPE_ANNEE :
            case Champ::TYPE_PERIODE :
            case Champ::TYPE_FORMATION :
        $texte .= $champ->getLibelle(). ' : ';
                if ($data !== 'null') {
                    $texte .= $data;
                }
                break;
            default :
                $texte .= 'Type ['. $champ->getType()->getCode() .'] inconnu !';
                break;
        }

        if (! $champ->estNonHistorise()) {
            $texte = "<span class='result-historiser'>".$texte."</span>";
        }

        $result  = '';
        $result .= '<div class="col-sm-offset-1 col-sm-11">';
        $result .= '<input type="checkbox" id="reponse_'.$champ->getId().'" name="reponse_'.$champ->getId().'" '. (($validation !== null)?'checked':'') . ' >';
        $result .= '<label for="reponse_'.$champ->getId().'">&nbsp;'. $texte . '</label>';
        $result .= '</div>';


        return $result;
    }
}