<?php

namespace UnicaenAutoform\View\Helper;

use UnicaenAutoform\Entity\Db\FormulaireInstance;
use UnicaenAutoform\Entity\Db\FormulaireReponse;
use UnicaenAutoform\Service\Champ\ChampServiceAwareTrait;
use Laminas\Form\View\Helper\AbstractHelper;
use Laminas\View\Renderer\PhpRenderer;
use Laminas\View\Resolver\TemplatePathStack;

class InstanceAsFormulaireHelper extends AbstractHelper
{
    use ChampServiceAwareTrait;

    private $debug = false;

    /**
     * @param FormulaireInstance $instance
     * @param ?string $url
     * @param ?FormulaireReponse[] $data
     * @param array $options
     * @return string
     *
     * -- OPTIONS ---------------------------------------
     * validation-top
     * validation-bottom
     * validation-floating
     * raccourci-top
     * raccourci-bottom
     */
    public function render(FormulaireInstance $instance, ?string $url = null, ?array $data = null, array $options = []) {
        $texte = "";

        /** @var PhpRenderer $view */
        $view = $this->getView();
        $view->resolver()->attach(new TemplatePathStack(['script_paths' => [__DIR__ . "/partial"]]));

        //$reponse = (isset($data[$champ->getId()]))?$data[$champ->getId()]->getReponse():null;
        $texte .= $view->partial('instance-as-formulaire', ['instance' => $instance, 'url' => $url, 'reponses' => $data, 'options' => $options]);
        return $texte;
    }
}