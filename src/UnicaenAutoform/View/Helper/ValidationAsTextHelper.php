<?php

namespace UnicaenAutoform\View\Helper;

use Application\Entity\Db\DemandeInstance;
use UnicaenAutoform\Entity\Db\Categorie;
use UnicaenAutoform\Entity\Db\Champ;
use UnicaenAutoform\Entity\Db\FormulaireInstance;
use UnicaenAutoform\Entity\Db\FormulaireReponse;
use UnicaenAutoform\Entity\Db\Validation;use Laminas\Form\View\Helper\AbstractHelper;
use UnicaenValidation\Entity\Db\ValidationInstance;

class ValidationAsTextHelper extends AbstractHelper
{
    /**
     * @param Validation $validation
     * @return string
     */
    public function render(FormulaireInstance $instance, ValidationInstance $validation) : string
    {
        $text = '';

        $formulaire = $instance->getFormulaire();
        $vreponses = json_decode($validation->getJustification());

        /** @var FormulaireReponse $reponse  */
        /** @var Champ $champ  */
        $dictionnaireCategorie = [];
        $dictionnaireChamp = [];
        foreach ($instance->getReponses() as $reponse) {
            $champ = $reponse->getChamp();
            $categorie = $champ->getCategorie();
            $dictionnaireCategorie[$categorie->getId()] = $categorie;
            $dictionnaireChamp[$categorie->getId()][] = $champ;
        }
        usort($dictionnaireCategorie, function (Categorie $a, Categorie $b) { return $a->getOrdre() <=> $b->getOrdre();});

        foreach ($dictionnaireCategorie as $categorie) {
            usort($dictionnaireChamp[$categorie->getId()], function (Champ $a, Champ $b) {
                return $a->getOrdre() <=> $b->getOrdre();
            });
            $textCategorie = "<h4>" . $categorie->getLibelle() . "</h4>";

            foreach ($dictionnaireChamp[$categorie->getId()] as $champ) {
                $vreponse = $vreponses->{'reponse_' . $champ->getId()} ?? null;
                if ($vreponse !== null) {
                    $texttmp = $this->getView()->champAsResult()->render($champ,
                        "on_LSF;on_LPC",
                        $vreponse);
                    $a=1;
                }

//                if ($reponse && $reponse->getReponse() !== null) {
//                $textCategorie .= $champ->getLibelle() . ":" . $vreponse->getReponse();
//                    $textCategorie .= $this->getView()->champAsValidation()->render($champ, ($reponses[$champ->getId()])->getReponse(), $reponses->{'reponse_'.$champ->getId()});
//                }
            }
            $text .= $textCategorie;
        }

//            /** @var Champ[] $champs */
//            $champs = $categorie->getChamps();
//            $champs = array_filter($champs, function (Champ $champ) { return $champ->estNonHistorise();});
//            usort($champs, function (Champ $a, Champ $b) { return $a->getOrdre() - $b->getOrdre();});
//
//            $results = [];
//            foreach ($champs as $champ) {
//                $fReponse = isset($fReponses[$champ->getId()])?$fReponses[$champ->getId()]->getReponse():null;
//                $vReponse = isset($vReponses[$champ->getId()])?$vReponses[$champ->getId()]->getValue():null;
//                if ($fReponse && $vReponse) $results[$champ->getId()] = $fReponses[$champ->getId()];
//            }
//
//            if (!empty($champs) && !empty($results)) {
////                $text .= '<li>';
//                $text .= '<h3>';
//                $text .= $categorie->getLibelle();
//                $text .= '</h3>';
//
//                $text .= '<ul>';
//                foreach ($champs as $champ) {
//                    if (isset($results[$champ->getId()])) {
//                        $text .= '<li>';
//                        switch($champ->getElement()) {
//                            case Champ::TYPE_MULTIPLE :
//                                $text .=  $this->getView()->champAsResult()->render($champ, $vReponses[$champ->getId()]->getValue());
//                                break;
//                            default :
//                                $text .=  $this->getView()->champAsResult()->render($champ, $fReponses[$champ->getId()]->getReponse());
//                                break;
//                        }
//                        $text .= '</li>';
//                    }
//                }
//                $text .= '</ul>';

//                $text .= '</li>';

//        }
//        $text .= '</ul>';
        return $text;
    }
}