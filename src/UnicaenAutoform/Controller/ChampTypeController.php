<?php

namespace UnicaenAutoform\Controller;

use Laminas\Mvc\Controller\AbstractActionController;
use Laminas\View\Model\ViewModel;
use UnicaenAutoform\Entity\Db\Champ;
use UnicaenAutoform\Form\ChampType\ChampTypeFormAwareTrait;
use UnicaenAutoform\Service\ChampType\ChampTypeServiceAwareTrait;
use UnicaenAutoform\Service\Formulaire\FormulaireServiceAwareTrait;

class ChampTypeController extends AbstractActionController
{
    use ChampTypeServiceAwareTrait;
    use FormulaireServiceAwareTrait;

    use ChampTypeFormAwareTrait;

    public function indexAction(): ViewModel
    {
        $champtypes = $this->getChampTypeService()->getChampTypes();

        return new ViewModel([
            'champtypes' => $champtypes
        ]);
    }

    public function afficherAction(): ViewModel
    {
        $champtype = $this->getChampTypeService()->getRequestedChampType($this, 'champ-type', true);

        $dictionnaire = [];
        foreach ($champtype->getChamps() as $champ) {
            $dictionnaire[$champ->getCategorie()->getFormulaire()->getCode()][] = $champ;
        }
        $formulaires = $this->getFormulaireService()->getFormulaires();

        $champ = new Champ();
        $champ->setId(31415);
        $champ->setLibelle($champtype->getExempleLibelle()??"Libellé");
        $champ->setType($champtype);
        $champ->setOptions($champtype->getExempleOptions());
        $champ->setTexte($champtype->getExempleTexte());

        return new ViewModel([
            'title' => "Affichage du type de champ [".$champtype->getCode()."]",
            'champtype' => $champtype,
            'dictionnaire' => $dictionnaire,
            'formulaires' => $formulaires,
            'champ' => $champ,
        ]);
    }


    public function modifierAction(): ViewModel
    {
        $champtype = $this->getChampTypeService()->getRequestedChampType($this, 'champ-type', true);

        $form = $this->getChampTypeForm();
        $form->setAttribute('action', $this->url()->fromRoute('autoform/champ-type/modifier', ['code' => $champtype->getCode()], [], true));
        $form->bind($champtype);

        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost();
            $form->setData($data);
            if ($form->isValid()) {
                $this->getChampTypeService()->update($champtype);
                exit();
            }
        }

        $vm = new ViewModel([
            'title' => "Modification du type de champ [".$champtype->getCode()."]",
            'form' => $form,
        ]);
        $vm->setTemplate('unicaen-autoform/default/default-form.phtml');
        return $vm;
    }
}