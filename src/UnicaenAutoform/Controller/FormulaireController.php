<?php

namespace UnicaenAutoform\Controller;

use JetBrains\PhpStorm\NoReturn;
use Laminas\Http\Request;
use Laminas\Http\Response;
use Laminas\Mvc\Controller\AbstractActionController;
use Laminas\View\Model\ViewModel;
use UnicaenAutoform\Entity\Db\Categorie;
use UnicaenAutoform\Entity\Db\Champ;
use UnicaenAutoform\Entity\Db\Formulaire;
use UnicaenAutoform\Entity\Db\FormulaireInstance;
use UnicaenAutoform\Form\Categorie\CategorieFormAwareTrait;
use UnicaenAutoform\Form\Champ\ChampFormAwareTrait;
use UnicaenAutoform\Form\Formulaire\FormulaireFormAwareTrait;
use UnicaenAutoform\Form\MotClef\MotClefFormAwareTrait;
use UnicaenAutoform\Service\Categorie\CategorieServiceAwareTrait;
use UnicaenAutoform\Service\Champ\ChampServiceAwareTrait;
use UnicaenAutoform\Service\Formulaire\FormulaireInstanceServiceAwareTrait;
use UnicaenAutoform\Service\Formulaire\FormulaireReponseServiceAwareTrait;
use UnicaenAutoform\Service\Formulaire\FormulaireServiceAwareTrait;

class FormulaireController extends AbstractActionController
{
    use CategorieServiceAwareTrait;
    use ChampServiceAwareTrait;
    use FormulaireServiceAwareTrait;
    use FormulaireReponseServiceAwareTrait;
    use FormulaireInstanceServiceAwareTrait;

    use CategorieFormAwareTrait;
    use ChampFormAwareTrait;
    use FormulaireFormAwareTrait;
    use MotClefFormAwareTrait;

    /** GESTION DES FORMULAIRES ***************************************************************************************/

    public function indexAction(): ViewModel
    {
        $formulaires = $this->getFormulaireService()->getFormulaires();

        return new ViewModel([
            'formulaires' => $formulaires,
        ]);
    }

    public function creerAction(): ViewModel
    {
        $formulaire = new Formulaire();

        $form = $this->getFormulaireForm();
        $form->setAttribute('action', $this->url()->fromRoute('autoform/creer-formulaire', [], [], true));
        $form->bind($formulaire);

        /** @var Request $request */
        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost();
            $form->setData($data);
            if ($form->isValid()) {
                $this->getFormulaireService()->create($formulaire);
            }
        }


        $vm = new ViewModel();
        $vm->setTemplate('unicaen-autoform/default/default-form');
        $vm->setVariables([
            'title' => 'Créer un nouveau formulaire',
            'form' => $form,
        ]);
        return $vm;
    }

    public function modifierAction(): ViewModel
    {
        /** @var Formulaire $formulaire */
        $formulaire = $this->getFormulaireService()->getRequestedFormulaire($this);

        return new ViewModel([
            'formulaire' => $formulaire,
        ]);
    }

    public function modifierDescriptionAction(): ViewModel
    {
        /** @var Formulaire $formulaire */
        $formulaire = $this->getFormulaireService()->getRequestedFormulaire($this);

        $form = $this->getFormulaireForm();
        $form->setAttribute('action', $this->url()->fromRoute('autoform/formulaire/modifier-description', ['formulaire' => $formulaire->getId()], [], true));
        $form->bind($formulaire);

        /** @var Request $request */
        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost();
            $form->setData($data);
            if ($form->isValid()) {
                $this->getFormulaireService()->update($formulaire);
            }
        }

        $vm = new ViewModel();
        $vm->setTemplate('unicaen-autoform/default/default-form');
        $vm->setVariables([
            'title' => 'Modifier la description du formulaire',
            'form' => $form,
        ]);
        return $vm;
    }

    public function historiserAction(): Response
    {
        /** @var Formulaire $formulaire */
        $formulaire = $this->getFormulaireService()->getRequestedFormulaire($this);
        $this->getFormulaireService()->historise($formulaire);

        return $this->redirect()->toRoute('autoform/formulaires', [], [], true);
    }

    public function restaurerAction(): Response
    {
        /** @var Formulaire $formulaire */
        $formulaire = $this->getFormulaireService()->getRequestedFormulaire($this);
        $this->getFormulaireService()->restore($formulaire);

        return $this->redirect()->toRoute('autoform/formulaires', [], [], true);
    }

    public function detruireAction(): Response
    {
        /** @var Formulaire $formulaire */
        $formulaire = $this->getFormulaireService()->getRequestedFormulaire($this);
        $this->getFormulaireService()->delete($formulaire);

        return $this->redirect()->toRoute('autoform/formulaires', [], [], true);
    }

    /** GESTION DES CATEGORIES ****************************************************************************************/

    public function ajouterCategorieAction(): ViewModel
    {
        $formulaire = $this->getFormulaireService()->getRequestedFormulaire($this);
        $categorie = new Categorie();

        $form = $this->getCategorieForm();
        $form->setAttribute('action', $this->url()->fromRoute('autoform/formulaire/modifier/ajouter-categorie', ['formulaire' => $formulaire->getId()], [], true));
        $form->bind($categorie);

        /** @var Request $request */
        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost();
            $form->setData($data);
            if ($form->isValid()) {
                $this->getFormulaireService()->compacter($formulaire);
                $categorie->setFormulaire($formulaire);
                $categorie->setOrdre(count($formulaire->getCategories()) + 1);
                $categorie->setCode($formulaire->getId() . "_" . uniqid());
                $this->getCategorieService()->create($categorie);
            }
        }

        $vm = new ViewModel();
        $vm->setTemplate('unicaen-autoform/default/default-form');
        $vm->setVariables([
            'title' => 'Ajouter une catégorie',
            'form' => $form,
        ]);
        return $vm;
    }

    public function modifierCategorieAction(): ViewModel
    {
        $formulaire = $this->getFormulaireService()->getRequestedFormulaire($this);
        $categorie = $this->getCategorieService()->getRequestedCategorie($this);

        $form = $this->getCategorieForm();
        $form->setAttribute('action', $this->url()->fromRoute('autoform/formulaire/modifier/modifier-categorie', ['formulaire' => $formulaire->getId()], [], true));
        $form->bind($categorie);

        /** @var Request $request */
        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost();
            $form->setData($data);
            if ($form->isValid()) {
                $this->getCategorieService()->update($categorie);
            }
        }

        $vm = new ViewModel();
        $vm->setTemplate('unicaen-autoform/default/default-form');
        $vm->setVariables([
            'title' => 'Modifier une catégorie',
            'form' => $form,
        ]);
        return $vm;
    }

    public function historiserCategorieAction(): Response
    {
        $formulaire = $this->getFormulaireService()->getRequestedFormulaire($this);
        $categorie = $this->getCategorieService()->getRequestedCategorie($this);
        $this->getCategorieService()->historise($categorie);

        return $this->redirect()->toRoute('autoform/formulaire/modifier', ['formulaire' => $formulaire->getId()], [], true);
    }

    public function restaurerCategorieAction(): Response
    {
        $formulaire = $this->getFormulaireService()->getRequestedFormulaire($this);
        $categorie = $this->getCategorieService()->getRequestedCategorie($this);
        $this->getCategorieService()->restore($categorie);

        return $this->redirect()->toRoute('autoform/formulaire/modifier', ['formulaire' => $formulaire->getId()], [], true);
    }

    public function detruireCategorieAction(): Response
    {
        $formulaire = $this->getFormulaireService()->getRequestedFormulaire($this);
        $categorie = $this->getCategorieService()->getRequestedCategorie($this);
        $this->getCategorieService()->delete($categorie);
        $this->getFormulaireService()->compacter($formulaire);

        return $this->redirect()->toRoute('autoform/formulaire/modifier', ['formulaire' => $formulaire->getId()], [], true);
    }

    public function bougerCategorieAction(): Response
    {
        $formulaire = $this->getFormulaireService()->getRequestedFormulaire($this);
        $categorie = $this->getCategorieService()->getRequestedCategorie($this);
        $direction = $this->params()->fromRoute('direction');

        $categories = $this->getCategorieService()->getCategoriesAvecSens($categorie, $direction);
        if ($categories && current($categories)) {
            $this->getCategorieService()->swapCategories($categorie, current($categories));
        }

        return $this->redirect()->toRoute('autoform/formulaire/modifier', ['formulaire' => $formulaire->getId()], [], true);
    }

    public function modifierMotsClefsCategorieAction(): ViewModel
    {
        $categorie = $this->getCategorieService()->getRequestedCategorie($this);

        $form = $this->getMotClefForm();
        $form->setAttribute('action', $this->url()->fromRoute('autoform/formulaire/modifier/modifier-mots-clefs-categorie', ['categorie' => $categorie->getId()], [], true));
        $form->bind($categorie);

        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost();
            $form->setData($data);
            if ($form->isValid()) {
                $this->getCategorieService()->update($categorie);
            }
        }

        $vm = new ViewModel();
        $vm->setTemplate('unicaen-autoform/default/default-form');
        $vm->setVariables([
            'title' => 'Modifier les mots-clefs de la catégorie [' . $categorie->getLibelle() . ']',
            'form' => $form,
        ]);
        return $vm;
    }

    #[NoReturn] public function swapCategorieAction(): void
    {
        $categorie1 = $this->getCategorieService()->getRequestedCategorie($this, 'categorie1');
        $categorie2 = $this->getCategorieService()->getRequestedCategorie($this, 'categorie2');

        $tmp = $categorie1->getOrdre();
        $categorie1->setOrdre($categorie2->getOrdre());
        $this->getCategorieService()->update($categorie1);
        $categorie2->setOrdre($tmp);
        $this->getCategorieService()->update($categorie2);

        exit();
    }

    /** GESTION DES CHAMPS ********************************************************************************************/

    public function ajouterChampAction(): ViewModel
    {
        $formulaire = $this->getFormulaireService()->getRequestedFormulaire($this);
        $categorie = $this->getCategorieService()->getRequestedCategorie($this);
        $champ = new Champ();

        $form = $this->getChampForm();
        $form->setAttribute('action', $this->url()->fromRoute('autoform/formulaire/categorie/ajouter-champ', ['formulaire' => $formulaire->getId(), 'categorie' => $categorie->getId()], [], true));
        $form->bind($champ);

        /** @var Request $request */
        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost();
            $form->setData($data);
            if ($form->isValid()) {
                $this->getCategorieService()->compacter($categorie);
                $champ->setCategorie($categorie);
                $champ->setOrdre(count($categorie->getChamps()) + 1);
                $champ->setCode($formulaire->getId() . "_" . $categorie->getId() . "_" . uniqid());
                $this->getChampService()->create($champ);
            }
        }

        $vm = new ViewModel();
        $vm->setTemplate('unicaen-autoform/formulaire/modifier-champ');
        $vm->setVariables([
            'title' => 'Ajouter un champ',
            'form' => $form,
        ]);
        return $vm;
    }

    public function modifierChampAction(): ViewModel
    {
        $formulaire = $this->getFormulaireService()->getRequestedFormulaire($this);
        $categorie = $this->getCategorieService()->getRequestedCategorie($this);
        $champ = $this->getChampService()->getRequestedChamp($this);

        $form = $this->getChampForm();
        $form->setAttribute('action', $this->url()->fromRoute('autoform/formulaire/categorie/champ/modifier', ['formulaire' => $formulaire->getId(), 'categorie' => $categorie->getId(), 'champ' => $champ->getId()], [], true));
        $form->bind($champ);

        /** @var Request $request */
        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost();
            $form->setData($data);
            if ($form->isValid()) {
                $this->getChampService()->update($champ);
            }
        }

        $vm = new ViewModel();
        $vm->setTemplate('unicaen-autoform/formulaire/modifier-champ');
        $vm->setVariables([
            'title' => 'Modifier un champ',
            'form' => $form,
        ]);
        return $vm;
    }

    public function historiserChampAction(): Response
    {
        $formulaire = $this->getFormulaireService()->getRequestedFormulaire($this);
        $champ = $this->getChampService()->getRequestedChamp($this);
        $this->getChampService()->historise($champ);

        return $this->redirect()->toRoute('autoform/formulaire/modifier', ['formulaire' => $formulaire->getId()], [], true);
    }

    public function restaurerChampAction(): Response
    {
        $formulaire = $this->getFormulaireService()->getRequestedFormulaire($this);
        $champ = $this->getChampService()->getRequestedChamp($this);
        $this->getChampService()->restore($champ);

        return $this->redirect()->toRoute('autoform/formulaire/modifier', ['formulaire' => $formulaire->getId()], [], true);
    }

    public function detruireChampAction(): Response
    {
        $formulaire = $this->getFormulaireService()->getRequestedFormulaire($this);
        $categorie = $this->getCategorieService()->getRequestedCategorie($this);
        $champ = $this->getChampService()->getRequestedChamp($this);
        $this->getChampService()->delete($champ);
        $this->getCategorieService()->compacter($categorie);

        return $this->redirect()->toRoute('autoform/formulaire/modifier', ['formulaire' => $formulaire->getId()], [], true);
    }

    public function bougerChampAction(): Response
    {
        $formulaire = $this->getFormulaireService()->getRequestedFormulaire($this);
        $champ = $this->getChampService()->getRequestedChamp($this);
        $direction = $this->params()->fromRoute('direction');

        $champs = $this->getChampService()->getChampsAvecSens($champ, $direction);
        if ($champs && current($champs)) {
            $this->getChampService()->swapChamps($champ, current($champs));
        }

        return $this->redirect()->toRoute('autoform/formulaire/modifier', ['formulaire' => $formulaire->getId()], [], true);
    }

    public function modifierMotsClefsChampAction(): ViewModel
    {
        $champ = $this->getChampService()->getRequestedChamp($this);

        $form = $this->getMotClefForm();
        $form->setAttribute('action', $this->url()->fromRoute('autoform/formulaire/modifier/modifier-mots-clefs-champ', ['champ' => $champ->getId()], [], true));
        $form->bind($champ);

        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost();
            $form->setData($data);
            if ($form->isValid()) {
                $this->getChampService()->update($champ);
            }
        }

        $vm = new ViewModel();
        $vm->setTemplate('unicaen-autoform/default/default-form');
        $vm->setVariables([
            'title' => 'Modifier les mots-clefs du champ [' . $champ->getLibelle() . ']',
            'form' => $form,
        ]);
        return $vm;
    }

    #[NoReturn] public function swapChampAction(): void
    {
        $champ1 = $this->getChampService()->getRequestedChamp($this, 'champ1');
        $champ2 = $this->getChampService()->getRequestedChamp($this, 'champ2');

        $tmp = $champ1->getOrdre();
        $champ1->setOrdre($champ2->getOrdre());
        $this->getChampService()->update($champ1);
        $champ2->setOrdre($tmp);
        $this->getChampService()->update($champ2);

        exit();
    }

    /** AFFICHAGES ****************************************************************************************************/

    public function afficherFormulaireAction(): ViewModel|Response
    {
        $formulaire = $this->getFormulaireService()->getRequestedFormulaire($this);
        $instance = $this->getFormulaireInstanceService()->getRequestedFormulaireInstance($this);
        $retour = $this->params()->fromQuery('retour');

        if ($instance === null) {
            $instance = new FormulaireInstance();
            $instance->setFormulaire($formulaire);
            $instance = $this->getFormulaireInstanceService()->create($instance);

            if ($retour) {
                return $this->redirect()->toUrl($retour);
            } else {
                return $this->redirect()->toRoute('autoform/formulaire/afficher-formulaire', ['formulaire' => $formulaire->getId(), 'instance' => $instance->getId()], [], true);
            }
        }

        /** @var Request $request */
        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost();
            $this->getFormulaireReponseService()->updateFormulaireReponse($formulaire, $instance, $data);
            if ($retour) {
                return $this->redirect()->toUrl($retour);
            } else {
                return $this->redirect()->toRoute('autoform/formulaire/afficher-formulaire', ['formulaire' => $formulaire->getId(), 'instance' => $instance->getId()], [], true);
            }
        }

        $reponses = $this->getFormulaireReponseService()->getFormulaireResponsesByFormulaireInstance($instance);
        $url = $this->url()->fromRoute('autoform/formulaire/afficher-formulaire', ['formulaire' => $formulaire->getId(), 'instance' => $instance->getId()], [], true);
        if ($retour) $url = $this->url()->fromRoute('autoform/formulaire/afficher-formulaire', ['formulaire' => $formulaire->getId(), 'instance' => $instance->getId()], ['query' => ['retour' => $retour]], true);

        return new ViewModel([
            'formulaire' => $formulaire,
            'instance' => $instance,
            'reponses' => $reponses,
            'url' => $url,
            'retour' => $retour,
        ]);
    }

    public function afficherResultatAction(): ViewModel
    {
        $formulaire = $this->getFormulaireService()->getRequestedFormulaire($this);
        $instance = $this->getFormulaireInstanceService()->getRequestedFormulaireInstance($this);
        $reponses = $this->getFormulaireReponseService()->getFormulaireResponsesByFormulaireInstance($instance);

        return new ViewModel([
            'formulaire' => $formulaire,
            'reponses' => $reponses,
        ]);
    }

    public function creerInstanceAction(): Response
    {
        $formulaire = $this->getFormulaireService()->getRequestedFormulaire($this);
        $instance = $this->getFormulaireInstanceService()->createInstance($formulaire->getCode());

        return $this->redirect()->toRoute('autoform/formulaire/afficher-formulaire', ['formulaire' => $formulaire->getId(), 'instance' => $instance->getId()], [], true);
    }
}