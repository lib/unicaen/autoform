<?php

namespace UnicaenAutoform\Controller;

use Laminas\Mvc\Controller\AbstractActionController;
use Laminas\View\Model\JsonModel;
use Laminas\View\Model\ViewModel;
use UnicaenUtilisateur\Service\User\UserServiceAwareTrait;

class IndexController extends AbstractActionController
{
    use UserServiceAwareTrait;

    public function indexAction(): ViewModel
    {
        return new ViewModel();
    }

    public function checkConnectionAction(): JsonModel
    {
        $user = $this->getUserService()->getConnectedUser();
        return new JsonModel(["connection" => $user !== null]);
    }
}