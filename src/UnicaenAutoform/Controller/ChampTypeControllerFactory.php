<?php

namespace UnicaenAutoform\Controller;

use Psr\Container\ContainerExceptionInterface;
use Psr\Container\ContainerInterface;
use Psr\Container\NotFoundExceptionInterface;
use UnicaenAutoform\Form\ChampType\ChampTypeForm;
use UnicaenAutoform\Service\ChampType\ChampTypeService;
use UnicaenAutoform\Service\Formulaire\FormulaireService;

class ChampTypeControllerFactory
{

    /**
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container): ChampTypeController
    {
        /**
         * @var ChampTypeService $champTypeService
         * @var FormulaireService $formulaireService
         * @var ChampTypeForm $champTypeForm
         */
        $champTypeService = $container->get(ChampTypeService::class);
        $formulaireService = $container->get(FormulaireService::class);
        $champTypeForm = $container->get('FormElementManager')->get(ChampTypeForm::class);

        $controller = new ChampTypeController();
        $controller->setChampTypeService($champTypeService);
        $controller->setFormulaireService($formulaireService);
        $controller->setChampTypeForm($champTypeForm);
        return $controller;
    }

}