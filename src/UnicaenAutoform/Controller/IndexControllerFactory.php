<?php

namespace UnicaenAutoform\Controller;

use Psr\Container\ContainerExceptionInterface;
use Psr\Container\ContainerInterface;
use Psr\Container\NotFoundExceptionInterface;
use UnicaenUtilisateur\Service\User\UserService;

class IndexControllerFactory
{

    /**
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container): IndexController
    {
        /** @var UserService $userService */
        $userService = $container->get(UserService::class);

        $controller = new IndexController();
        $controller->setUserService($userService);
        return $controller;
    }
}