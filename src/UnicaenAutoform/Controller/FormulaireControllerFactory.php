<?php

namespace UnicaenAutoform\Controller;

use Psr\Container\ContainerExceptionInterface;
use Psr\Container\ContainerInterface;
use Psr\Container\NotFoundExceptionInterface;
use UnicaenAutoform\Form\Categorie\CategorieForm;
use UnicaenAutoform\Form\Champ\ChampForm;
use UnicaenAutoform\Form\Formulaire\FormulaireForm;
use UnicaenAutoform\Form\MotClef\MotClefForm;
use UnicaenAutoform\Service\Categorie\CategorieService;
use UnicaenAutoform\Service\Champ\ChampService;
use UnicaenAutoform\Service\Formulaire\FormulaireInstanceService;
use UnicaenAutoform\Service\Formulaire\FormulaireReponseService;
use UnicaenAutoform\Service\Formulaire\FormulaireService;

class FormulaireControllerFactory
{

    /**
     * @param ContainerInterface $container
     * @return FormulaireController
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container): FormulaireController
    {
        /**
         * @var CategorieService $categorieService
         * @var ChampService $champService
         * @var FormulaireService $formulaireService
         * @var FormulaireReponseService $formulaireReponseService
         * @var FormulaireInstanceService $formulaireInstanceService
         */
        $categorieService = $container->get(CategorieService::class);
        $champService = $container->get(ChampService::class);
        $formulaireService = $container->get(FormulaireService::class);
        $formulaireReponseService = $container->get(FormulaireReponseService::class);
        $formulaireInstanceService = $container->get(FormulaireInstanceService::class);

        /**
         * @var CategorieForm $categorieForm
         * @var ChampForm $champForm
         * @var FormulaireForm $formulaireForm
         * @var MotClefForm $motClefForm
         */
        $categorieForm = $container->get('FormElementManager')->get(CategorieForm::class);
        $champForm = $container->get('FormElementManager')->get(ChampForm::class);
        $formulaireForm = $container->get('FormElementManager')->get(FormulaireForm::class);
        $motClefForm = $container->get('FormElementManager')->get(MotClefForm::class);

        $controller = new FormulaireController();
        $controller->setCategorieService($categorieService);
        $controller->setChampService($champService);
        $controller->setFormulaireService($formulaireService);
        $controller->setFormulaireReponseService($formulaireReponseService);
        $controller->setFormulaireInstanceService($formulaireInstanceService);
        $controller->setCategorieForm($categorieForm);
        $controller->setChampForm($champForm);
        $controller->setFormulaireForm($formulaireForm);
        $controller->setMotClefForm($motClefForm);
        return $controller;
    }
}