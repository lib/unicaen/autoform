<?php

namespace UnicaenAutoform\Provider\Privilege;

use UnicaenPrivilege\Provider\Privilege\Privileges;

class AutoformindexPrivileges extends Privileges
{
    const INDEX = 'autoformindex-index';
}
