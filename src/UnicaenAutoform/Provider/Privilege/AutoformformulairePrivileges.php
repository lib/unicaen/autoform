<?php

namespace UnicaenAutoform\Provider\Privilege;

use UnicaenPrivilege\Provider\Privilege\Privileges;

class AutoformformulairePrivileges extends Privileges
{
    const FORMULAIRE_INDEX = 'autoformformulaire-formulaire_index';
    const FORMULAIRE_AFFICHER = 'autoformformulaire-formulaire_afficher';
    const FORMULAIRE_AJOUTER = 'autoformformulaire-formulaire_ajouter';
    const FORMULAIRE_MODIFIER = 'autoformformulaire-formulaire_modifier';
    const FORMULAIRE_HISTORISER = 'autoformformulaire-formulaire_historiser';
    const FORMULAIRE_SUPPRIMER = 'autoformformulaire-formulaire_supprimer';
}
