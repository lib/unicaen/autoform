<?php

namespace UnicaenAutoform\Provider\Privilege;

use UnicaenPrivilege\Provider\Privilege\Privileges;

class ChamptypePrivileges extends Privileges
{
    const CHAMPTYPE_INDEX = 'champtype-champtype_index';
    const CHAMPTYPE_AFFICHER = 'champtype-champtype_afficher';
    const CHAMPTYPE_MODIFIER = 'champtype-champtype_modifier';
}