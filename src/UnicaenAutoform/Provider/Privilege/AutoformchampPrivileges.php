<?php

namespace UnicaenAutoform\Provider\Privilege;

use UnicaenPrivilege\Provider\Privilege\Privileges;

class AutoformchampPrivileges extends Privileges
{
    const CHAMP_INDEX = 'autoformchamp-champ_index';
    const CHAMP_AFFICHER = 'autoformchamp-champ_afficher';
    const CHAMP_AJOUTER = 'autoformchamp-champ_ajouter';
    const CHAMP_MODIFIER = 'autoformchamp-champ_modifier';
    const CHAMP_HISTORISER = 'autoformchamp-champ_historiser';
    const CHAMP_SUPPRIMER = 'autoformchamp-champ_supprimer';
}