<?php

namespace UnicaenAutoform\Provider\Privilege;

use UnicaenPrivilege\Provider\Privilege\Privileges;

class AutoformcategoriePrivileges extends Privileges
{
    const CATEGORIEF_INDEX = 'autoformcategorie-categorief_index';
    const CATEGORIEF_AFFICHER = 'autoformcategorie-categorief_afficher';
    const CATEGORIEF_AJOUTER = 'autoformcategorie-categorief_ajouter';
    const CATEGORIEF_MODIFIER = 'autoformcategorie-categorief_modifier';
    const CATEGORIEF_HISTORISER = 'autoformcategorie-categorief_historiser';
    const CATEGORIEF_SUPPRIMER = 'autoformcategorie-categorief_supprimer';
}