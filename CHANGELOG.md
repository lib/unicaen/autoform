Changement
---

# Version 6

## 6.2.0 - 25/02/2025
- Modernisation des services
- Ajout d'une interface de présentation des différents champs
- Ajout du nouveau type de champ "Plus custom"

## 6.1.5 - 18/02/2025
- Nouveau element "Plus texte"
- Possibilité de manipuler en bac à sable les formaulaire depuis l'interface d'administration

**6.1.4**
- Correction du lien en cas de déconnection  

**v6.0.3**
- Retrait de la partie validation (utiliser plutôt unicaen/validation)
- Label plus complet pour les ViewHelpers

**v6.0.2**
- Nettoyage du code (vers php 8.0 + code sniffing)

**v6.0.1**
- Ajout des fichiers SQL
- Debut d'implémentation du drag end drop

**v6.0.0**
- Version compatible php 8
