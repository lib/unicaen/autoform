<?php

namespace UnicaenAutoform;

use UnicaenAutoform\Form\Champ\ChampForm;
use UnicaenAutoform\Form\Champ\ChampFormFactory;
use UnicaenAutoform\Form\Champ\ChampHydrator;
use UnicaenAutoform\Form\Champ\ChampHydratorFactory;
use UnicaenAutoform\Service\Champ\ChampService;
use UnicaenAutoform\Service\Champ\ChampServiceFactory;
use UnicaenAutoform\View\Helper\ChampAsInputHelperFactory;
use UnicaenAutoform\View\Helper\ChampAsResultHelperFactory;
use UnicaenAutoform\View\Helper\ChampAsValidationHelper;
use UnicaenAutoform\View\Helper\InstanceAsDivHelper;
use UnicaenAutoform\View\Helper\InstanceAsFormulaireHelper;
use UnicaenAutoform\View\Helper\InstanceAsTextHelper;
use UnicaenAutoform\View\Helper\ValidationAsTextHelper;
use UnicaenPrivilege\Guard\PrivilegeController;

return [
    'bjyauthorize' => [
        'guards' => [
            PrivilegeController::class => [],
        ],
    ],

    'router' => [
        'routes' => [
        ],
    ],

    'service_manager' => [
        'factories' => [
            ChampService::class => ChampServiceFactory::class,
        ],
    ],
    'controllers'     => [
        'factories' => [],
    ],
    'form_elements' => [
        'factories' => [
            ChampForm::class => ChampFormFactory::class,
        ],
    ],
    'hydrators' => [
        'factories' => [
            ChampHydrator::class => ChampHydratorFactory::class,
        ],
    ],

    'view_helpers' => [
        'invokables' => [
            //'champAsResult'              => ChampAsResultHelper::class,
            //'champAsInput'               => ChampAsInputHelper::class,
            'champAsValidation'          => ChampAsValidationHelper::class,
            'instanceAsText'             => InstanceAsTextHelper::class,
            'instanceAsDiv'              => InstanceAsDivHelper::class,
            'validationAsText'            => ValidationAsTextHelper::class,
            'instanceAsFormulaire'       => InstanceAsFormulaireHelper::class,
        ],
        'factories' => [
            'champAsInput'              => ChampAsInputHelperFactory::class,
            'champAsResult'             => ChampAsResultHelperFactory::class,
        ],
    ],

];