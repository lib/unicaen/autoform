<?php

namespace UnicaenAutoform;

use UnicaenAutoform\Controller\FormulaireController;
use UnicaenAutoform\Controller\FormulaireControllerFactory;
use UnicaenAutoform\Form\Categorie\CategorieForm;
use UnicaenAutoform\Form\Categorie\CategorieFormFactory;
use UnicaenAutoform\Form\Categorie\CategorieHydrator;
use UnicaenAutoform\Form\Categorie\CategorieHydratorFactory;
use UnicaenAutoform\Form\Formulaire\FormulaireForm;
use UnicaenAutoform\Form\Formulaire\FormulaireFormFactory;
use UnicaenAutoform\Form\Formulaire\FormulaireHydrator;
use UnicaenAutoform\Form\Formulaire\FormulaireHydratorFactory;
use UnicaenAutoform\Provider\Privilege\AutoformformulairePrivileges;
use UnicaenAutoform\Service\Categorie\CategorieService;
use UnicaenAutoform\Service\Categorie\CategorieServiceFactory;
use UnicaenAutoform\Service\Formulaire\FormulaireInstanceService;
use UnicaenAutoform\Service\Formulaire\FormulaireInstanceServiceFactory;
use UnicaenAutoform\Service\Formulaire\FormulaireReponseService;
use UnicaenAutoform\Service\Formulaire\FormulaireReponseServiceFactory;
use UnicaenAutoform\Service\Formulaire\FormulaireService;
use UnicaenAutoform\Service\Formulaire\FormulaireServiceFactory;
use UnicaenPrivilege\Guard\PrivilegeController;
use Laminas\Router\Http\Literal;
use Laminas\Router\Http\Segment;

return [
    'bjyauthorize' => [
        'guards' => [
            PrivilegeController::class => [
                [
                    'controller' => FormulaireController::class,
                    'action' => [
                        'index',
                    ],
                    'privileges' => [
                        AutoformformulairePrivileges::FORMULAIRE_INDEX,
                    ],
                ],
                [
                    'controller' => FormulaireController::class,
                    'action' => [
                        'afficher-formulaire',
                        'afficher-resultat',
                        'export-pdf',
                    ],
                    'privileges' => [
                        AutoformformulairePrivileges::FORMULAIRE_AFFICHER,
                    ],
                ],
                [
                    'controller' => FormulaireController::class,
                    'action' => [
                        'creer',
                    ],
                    'privileges' => [
                        AutoformformulairePrivileges::FORMULAIRE_AJOUTER,
                    ],
                ],
                [
                    'controller' => FormulaireController::class,
                    'action' => [
                        'modifier',
                        'modifier-description',
                        'ajouter-categorie',
                        'modifier-categorie',
                        'historiser-categorie',
                        'restaurer-categorie',
                        'detruire-categorie',
                        'bouger-categorie',
                        'modifier-mots-clefs-categorie',
                        'modifier-mots-clefs-champ',
                        'swap-categorie',

                        'ajouter-champ',
                        'modifier-champ',
                        'historiser-champ',
                        'restaurer-champ',
                        'detruire-champ',
                        'bouger-champ',
                        'swap-champ',

                        'creer-instance'
                    ],
                    'privileges' => [
                        AutoformformulairePrivileges::FORMULAIRE_MODIFIER,
                    ],
                ],
                [
                    'controller' => FormulaireController::class,
                    'action' => [
                        'historiser',
                        'restaurer',
                    ],
                    'privileges' => [
                        AutoformformulairePrivileges::FORMULAIRE_HISTORISER,
                    ],
                ],
                [
                    'controller' => FormulaireController::class,
                    'action' => [
                        'detruire',
                    ],
                    'privileges' => [
                        AutoformformulairePrivileges::FORMULAIRE_SUPPRIMER,
                    ],
                ],
            ],
        ],
    ],

    'router' => [
        'routes' => [
            'autoform' => [
                'child_routes' => [
                    'formulaires' => [
                        'type' => Literal::class,
                        'may_terminate' => true,
                        'options' => [
                            'route'    => '/formulaires',
                            'defaults' => [
                                'controller' => FormulaireController::class,
                                'action'     => 'index',
                            ],
                        ],
                    ],
                    'creer-formulaire' => [
                        'type' => Literal::class,
                        'may_terminate' => true,
                        'options' => [
                            'route'    => '/creer-formulaire',
                            'defaults' => [
                                'controller' => FormulaireController::class,
                                'action'     => 'creer',
                            ],
                        ],
                    ],

                    'formulaire' => [
                        'type' => Segment::class,
                        'may_terminate' => false,
                        'options' => [
                            'route'    => '/formulaire/:formulaire',
                        ],
                        'child_routes' => [
                            'afficher-formulaire' => [
                                'type' => Segment::class,
                                'may_terminate' => true,
                                'options' => [
                                    'route'    => '/afficher-formulaire[/:instance]',
                                    'defaults' => [
                                        'controller' => FormulaireController::class,
                                        'action'     => 'afficher-formulaire',
                                    ],
                                ],
                            ],
                            'afficher-resultat' => [
                                'type' => Segment::class,
                                'may_terminate' => true,
                                'options' => [
                                    'route'    => '/afficher-resultat[/:instance]',
                                    'defaults' => [
                                        'controller' => FormulaireController::class,
                                        'action'     => 'afficher-resultat',
                                    ],
                                ],
                            ],
                            'modifier-description' => [
                                'type' => Literal::class,
                                'may_terminate' => true,
                                'options' => [
                                    'route'    => '/modifier-description',
                                    'defaults' => [
                                        'controller' => FormulaireController::class,
                                        'action'     => 'modifier-description',
                                    ],
                                ],
                            ],
                            'creer-instance' => [
                                'type' => Segment::class,
                                'may_terminate' => true,
                                'options' => [
                                    'route'    => '/creer-instance',
                                    'defaults' => [
                                        /** @see FormulaireController::creerInstanceAction() */
                                        'controller' => FormulaireController::class,
                                        'action'     => 'creer-instance',
                                    ],
                                ],
                            ],
                            'modifier' => [
                                'type' => Literal::class,
                                'may_terminate' => true,
                                'options' => [
                                    'route'    => '/modifier',
                                    'defaults' => [
                                        'controller' => FormulaireController::class,
                                        'action'     => 'modifier',
                                    ],
                                ],
                                'child_routes' => [
                                    'ajouter-categorie' => [
                                        'type' => Literal::class,
                                        'may_terminate' => true,
                                        'options' => [
                                            'route'    => '/ajouter-categorie',
                                            'defaults' => [
                                                'controller' => FormulaireController::class,
                                                'action'     => 'ajouter-categorie',
                                            ],
                                        ],
                                    ],
                                    'modifier-categorie' => [
                                        'type' => Segment::class,
                                        'may_terminate' => true,
                                        'options' => [
                                            'route'    => '/modifier-categorie/:categorie',
                                            'defaults' => [
                                                'controller' => FormulaireController::class,
                                                'action'     => 'modifier-categorie',
                                            ],
                                        ],
                                    ],
                                    'historiser-categorie' => [
                                        'type' => Segment::class,
                                        'may_terminate' => true,
                                        'options' => [
                                            'route'    => '/historiser-categorie/:categorie',
                                            'defaults' => [
                                                'controller' => FormulaireController::class,
                                                'action'     => 'historiser-categorie',
                                            ],
                                        ],
                                    ],
                                    'restaurer-categorie' => [
                                        'type' => Segment::class,
                                        'may_terminate' => true,
                                        'options' => [
                                            'route'    => '/restaurer-categorie/:categorie',
                                            'defaults' => [
                                                'controller' => FormulaireController::class,
                                                'action'     => 'restaurer-categorie',
                                            ],
                                        ],
                                    ],
                                    'detruire-categorie' => [
                                        'type' => Segment::class,
                                        'may_terminate' => true,
                                        'options' => [
                                            'route'    => '/detruire-categorie/:categorie',
                                            'defaults' => [
                                                'controller' => FormulaireController::class,
                                                'action'     => 'detruire-categorie',
                                            ],
                                        ],
                                    ],
                                    'bouger-categorie' => [
                                        'type' => Segment::class,
                                        'may_terminate' => true,
                                        'options' => [
                                            'route'    => '/bouger-categorie/:categorie/:direction',
                                            'defaults' => [
                                                'controller' => FormulaireController::class,
                                                'action'     => 'bouger-categorie',
                                            ],
                                        ],
                                    ],
                                    'modifier-mots-clefs-categorie' => [
                                        'type' => Segment::class,
                                        'may_terminate' => true,
                                        'options' => [
                                            'route'    => '/modifier-mots-clefs-categorie/:categorie',
                                            'defaults' => [
                                                /** @see FormulaireController::modifierMotsClefsCategorieAction() */
                                                'controller' => FormulaireController::class,
                                                'action'     => 'modifier-mots-clefs-categorie',
                                            ],
                                        ],
                                    ],
                                    'swap-categorie' => [
                                        'type' => Segment::class,
                                        'may_terminate' => true,
                                        'options' => [
                                            'route'    => '/swap-categorie/:categorie1/:categorie2',
                                            'defaults' => [
                                                /** @see FormulaireController::swapCategorieAction() */
                                                'controller' => FormulaireController::class,
                                                'action'     => 'swap-categorie',
                                            ],
                                        ],
                                    ],
                                    'swap-champ' => [
                                        'type' => Segment::class,
                                        'may_terminate' => true,
                                        'options' => [
                                            'route'    => '/swap-champ/:champ1/:champ2',
                                            'defaults' => [
                                                /** @see FormulaireController::swapChampAction() */
                                                'controller' => FormulaireController::class,
                                                'action'     => 'swap-champ',
                                            ],
                                        ],
                                    ],
                                    'modifier-mots-clefs-champ' => [
                                        'type' => Segment::class,
                                        'may_terminate' => true,
                                        'options' => [
                                            'route'    => '/modifier-mots-clefs-champ/:champ',
                                            'defaults' => [
                                                /** @see FormulaireController::modifierMotsClefsChampAction() */
                                                'controller' => FormulaireController::class,
                                                'action'     => 'modifier-mots-clefs-champ',
                                            ],
                                        ],
                                    ],
                                ],
                            ],
                            'historiser' => [
                                'type' => Literal::class,
                                'may_terminate' => true,
                                'options' => [
                                    'route'    => '/historiser',
                                    'defaults' => [
                                        'controller' => FormulaireController::class,
                                        'action'     => 'historiser',
                                    ],
                                ],
                            ],
                            'restaurer' => [
                                'type' => Literal::class,
                                'may_terminate' => true,
                                'options' => [
                                    'route'    => '/restaurer',
                                    'defaults' => [
                                        'controller' => FormulaireController::class,
                                        'action'     => 'restaurer',
                                    ],
                                ],
                            ],
                            'detruire' => [
                                'type' => Literal::class,
                                'may_terminate' => true,
                                'options' => [
                                    'route'    => '/detruire',
                                    'defaults' => [
                                        'controller' => FormulaireController::class,
                                        'action'     => 'detruire',
                                    ],
                                ],
                            ],

                            'categorie' => [
                                'type' => Segment::class,
                                'may_terminate' => false,
                                'options' => [
                                    'route'    => '/categorie/:categorie',
                                ],
                                'child_routes' => [
                                    'ajouter-champ' => [
                                        'type' => Literal::class,
                                        'may_terminate' => true,
                                        'options' => [
                                            'route'    => '/ajouter-champ',
                                            'defaults' => [
                                                'controller' => FormulaireController::class,
                                                'action'     => 'ajouter-champ',
                                            ],
                                        ],
                                    ],
                                    'champ' => [
                                        'type' => Segment::class,
                                        'may_terminate' => false,
                                        'options' => [
                                            'route'    => '/champ/:champ',
                                        ],
                                        'child_routes' => [
                                            'modifier' => [
                                                'type' => Literal::class,
                                                'may_terminate' => true,
                                                'options' => [
                                                    'route'    => '/modifier',
                                                    'defaults' => [
                                                        'controller' => FormulaireController::class,
                                                        'action'     => 'modifier-champ',
                                                    ],
                                                ],
                                            ],
                                            'bouger' => [
                                                'type' => Segment::class,
                                                'may_terminate' => true,
                                                'options' => [
                                                    'route'    => '/bouger/:direction',
                                                    'defaults' => [
                                                        'controller' => FormulaireController::class,
                                                        'action'     => 'bouger-champ',
                                                    ],
                                                ],
                                            ],
                                            'historiser' => [
                                                'type' => Literal::class,
                                                'may_terminate' => true,
                                                'options' => [
                                                    'route'    => '/historiser',
                                                    'defaults' => [
                                                        'controller' => FormulaireController::class,
                                                        'action'     => 'historiser-champ',
                                                    ],
                                                ],
                                            ],
                                            'restaurer' => [
                                                'type' => Literal::class,
                                                'may_terminate' => true,
                                                'options' => [
                                                    'route'    => '/restaurer',
                                                    'defaults' => [
                                                        'controller' => FormulaireController::class,
                                                        'action'     => 'restaurer-champ',
                                                    ],
                                                ],
                                            ],
                                            'detruire' => [
                                                'type' => Literal::class,
                                                'may_terminate' => true,
                                                'options' => [
                                                    'route'    => '/detruire',
                                                    'defaults' => [
                                                        'controller' => FormulaireController::class,
                                                        'action'     => 'detruire-champ',
                                                    ],
                                                ],
                                            ],
                                        ],
                                    ],
                                ],
                            ],
                        ],
                    ],
                ],
            ],
        ],
    ],

    'service_manager' => [
        'factories' => [
            CategorieService::class => CategorieServiceFactory::class,
            FormulaireService::class => FormulaireServiceFactory::class,
            FormulaireInstanceService::class => FormulaireInstanceServiceFactory::class,
            FormulaireReponseService::class => FormulaireReponseServiceFactory::class,
        ],
    ],
    'controllers'     => [
        'factories' => [
            FormulaireController::class => FormulaireControllerFactory::class,
        ],
    ],
    'form_elements' => [
        'factories' => [
            CategorieForm::class => CategorieFormFactory::class,
            FormulaireForm::class => FormulaireFormFactory::class,
        ],
    ],
    'hydrators' => [
        'factories' => [
            CategorieHydrator::class => CategorieHydratorFactory::class,
            FormulaireHydrator::class => FormulaireHydratorFactory::class,
        ],
    ],

    'view_helpers' => [
        'invokables' => [
        ],
    ],

];