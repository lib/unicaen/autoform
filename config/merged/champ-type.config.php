<?php

namespace UnicaenAutoform;

use Laminas\Router\Http\Literal;
use Laminas\Router\Http\Segment;
use UnicaenAutoform\Controller\ChampTypeController;
use UnicaenAutoform\Controller\ChampTypeControllerFactory;
use UnicaenAutoform\Form\ChampType\ChampTypeForm;
use UnicaenAutoform\Form\ChampType\ChampTypeFormFactory;
use UnicaenAutoform\Form\ChampType\ChampTypeHydrator;
use UnicaenAutoform\Form\ChampType\ChampTypeHydratorFactory;
use UnicaenAutoform\Provider\Privilege\AutoformformulairePrivileges;
use UnicaenAutoform\Provider\Privilege\ChamptypePrivileges;
use UnicaenAutoform\Service\ChampType\ChampTypeService;
use UnicaenAutoform\Service\ChampType\ChampTypeServiceFactory;
use UnicaenPrivilege\Guard\PrivilegeController;

return [
    'bjyauthorize' => [
        'guards' => [
            PrivilegeController::class => [
                [
                    'controller' => ChampTypeController::class,
                    'action' => [
                        'index',
                    ],
                    'privileges' => [
                        ChamptypePrivileges::CHAMPTYPE_INDEX,
                    ],
                ],
                [
                    'controller' => ChampTypeController::class,
                    'action' => [
                        'afficher',
                    ],
                    'privileges' => [
                        ChamptypePrivileges::CHAMPTYPE_AFFICHER,
                    ],
                ],
                [
                    'controller' => ChampTypeController::class,
                    'action' => [
                        'modifier',
                    ],
                    'privileges' => [
                        ChamptypePrivileges::CHAMPTYPE_MODIFIER,
                    ],
                ],
            ],
        ],
    ],

    'router' => [
        'routes' => [
            'autoform' => [
                'child_routes' => [
                    'champ-type' => [
                        'type' => Literal::class,
                        'may_terminate' => true,
                        'options' => [
                            'route' => '/champ-type',
                            'defaults' => [
                                /** @see ChampTypeController::indexAction() */
                                'controller' => ChampTypeController::class,
                                'action' => 'index',
                            ],
                        ],
                        'child_routes' => [
                            'afficher' => [
                                'type' => Segment::class,
                                'may_terminate' => true,
                                'options' => [
                                    'route' => '/afficher/:champ-type',
                                    'defaults' => [
                                        /** @see ChampTypeController::afficherAction() */
                                        'action' => 'afficher',
                                    ],
                                ],
                            ],
                            'modifier' => [
                                'type' => Segment::class,
                                'may_terminate' => true,
                                'options' => [
                                    'route' => '/modifier/:champ-type',
                                    'defaults' => [
                                        /** @see ChampTypeController::modifierAction() */
                                        'action' => 'modifier',
                                    ],
                                ],
                            ],
                        ],
                    ],
                ],
            ],
        ],
    ],

    'service_manager' => [
        'factories' => [
            ChampTypeService::class => ChampTypeServiceFactory::class,
        ],
    ],
    'controllers' => [
        'factories' => [
            ChampTypeController::class => ChampTypeControllerFactory::class,
        ],
    ],
    'form_elements' => [
        'factories' => [
            ChampTypeForm::class => ChampTypeFormFactory::class,
        ],
    ],
    'hydrators' => [
        'factories' => [
            ChampTypeHydrator::class => ChampTypeHydratorFactory::class,
        ],
    ],

    'view_helpers' => [
        'invokables' => [
        ],
        'factories' => [
        ],
    ],

];