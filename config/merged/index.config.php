<?php

namespace UnicaenAutoform;

use UnicaenAutoform\Controller\IndexController;
use UnicaenAutoform\Controller\IndexControllerFactory;
use UnicaenAutoform\Form\MotClef\MotClefForm;
use UnicaenAutoform\Form\MotClef\MotClefFormFactory;
use UnicaenAutoform\Form\MotClef\MotClefHydrator;
use UnicaenAutoform\Form\MotClef\MotClefHydratorFactory;
use UnicaenAutoform\Provider\Privilege\AutoformindexPrivileges;
use UnicaenPrivilege\Guard\PrivilegeController;
use Laminas\Router\Http\Literal;

return [
    'bjyauthorize' => [
        'guards' => [
            PrivilegeController::class => [
                [
                    'controller' => IndexController::class,
                    'action' => [
                        'index',
                    ],
                    'privileges' => [
                        AutoformindexPrivileges::INDEX,
                    ],
                ],
                [
                    'controller' => IndexController::class,
                    'action' => [
                        'check-connection',
                    ],
                    'roles' => [],
                ],
            ],
        ],
    ],

    'router' => [
        'routes' => [
            'autoform' => [
                'type' => Literal::class,
                'may_terminate' => true,
                'options' => [
                    'route'    => '/autoform',
                    'defaults' => [
                        'controller' => IndexController::class,
                        'action'     => 'index',
                    ],
                ],
                'child_routes' => [
                    'check-connection' => [
                        'type' => Literal::class,
                        'may_terminate' => true,
                        'options' => [
                            'route' => '/check-connection',
                            'defaults' => [
                                'controller' => IndexController::class,
                                'action' => 'check-connection',
                            ],
                        ],
                    ],
                ],
            ],
        ],
    ],

    'service_manager' => [
        'factories' => [],
    ],
    'controllers'     => [
        'factories' => [
            IndexController::class => IndexControllerFactory::class,
        ],
    ],
    'form_elements' => [
        'factories' => [
            MotClefForm::class => MotClefFormFactory::class,
        ],
    ],
    'hydrators' => [
        'factories' => [
            MotClefHydrator::class => MotClefHydratorFactory::class,
        ],
    ]

];