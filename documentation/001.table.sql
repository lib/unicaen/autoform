create table unicaen_autoform_formulaire
(
    id                    serial not null
        constraint autoform_formulaire_pk primary key,
    libelle               varchar(128)                                                    not null,
    description           varchar(2048),
    histo_creation        timestamp                                                       not null,
    histo_createur_id     integer                                                         not null
        constraint composante_createur_fk references unicaen_utilisateur_user,
    histo_modification    timestamp                                                       not null,
    histo_modificateur_id integer                                                         not null
        constraint composante_modificateur_fk references unicaen_utilisateur_user,
    histo_destruction     timestamp,
    histo_destructeur_id  integer
        constraint composante_destructeur_fk references unicaen_utilisateur_user,
    code                  varchar(256)
);
create unique index autoform_formulaire_id_uindex on unicaen_autoform_formulaire (id);

create table unicaen_autoform_categorie
(
    id                    serial not null
        constraint autoform_categorie_pk primary key,
    code                  varchar(64)                                                    not null,
    libelle               varchar(256)                                                   not null,
    ordre                 integer default 10000                                          not null,
    formulaire            integer                                                        not null
        constraint autoform_categorie_formulaire_fk references unicaen_autoform_formulaire (id)
        on delete cascade,
    mots_clefs            varchar(1024),
    histo_creation        timestamp                                                      not null,
    histo_createur_id     integer                                                        not null
        constraint composante_createur_fk references unicaen_utilisateur_user,
    histo_modification    timestamp                                                      not null,
    histo_modificateur_id integer                                                        not null
        constraint composante_modificateur_fk references unicaen_utilisateur_user,
    histo_destruction     timestamp,
    histo_destructeur_id  integer
        constraint composante_destructeur_fk references unicaen_utilisateur_user
);
create unique index autoform_categorie_code_uindex on unicaen_autoform_categorie (code);
create unique index autoform_categorie_id_uindex on unicaen_autoform_categorie (id);

create table unicaen_autoform_champ_type
(
    code        varchar(256)  not null
        constraint unicaen_autoform_champ_type_pk primary key,
    libelle     varchar(1024) not null,
    description text,
    usage text,
    example_libelle varchar(2048),
    example_options varchar(2048),
    example_texte varchar(2048),
    example_reponse varchar(2048)
);
create unique index unicaen_autoform_champ_type_code_uindex on unicaen_autoform_champ_type (code);



create table unicaen_autoform_champ
(
    id                    serial not null
        constraint autoform_champ_pk primary key,
    categorie             integer                                                    not null
        constraint autoform_champ_categorie_fk references unicaen_autoform_categorie
        on delete cascade,
    code                  varchar(64)                                                not null,
    libelle               varchar(256)                                               not null,
    texte                 varchar(256)                                               not null,
    ordre                 integer default 10000                                      not null,
    type_id               varchar(256),
    balise                boolean,
    options               varchar(1024),
    mots_clefs            varchar(1024),
    histo_creation        timestamp                                                  not null,
    histo_createur_id     integer                                                    not null
        constraint composante_createur_fk references unicaen_utilisateur_user,
    histo_modification    timestamp                                                  not null,
    histo_modificateur_id integer                                                    not null
        constraint composante_modificateur_fk references unicaen_utilisateur_user,
    histo_destruction     timestamp,
    histo_destructeur_id  integer
        constraint composante_destructeur_fk references unicaen_utilisateur_user
);
alter table unicaen_autoform_champ add constraint unicaen_autoform_champ_unicaen_autoform_champ_type_code_fk
        foreign key (type_id) references unicaen_autoform_champ_type on delete cascade;
create unique index autoform_champ_id_uindex on unicaen_autoform_champ (id);

create table unicaen_autoform_formulaire_instance
(
    id                    serial not null
        constraint autoform_formulaire_instance_pk primary key,
    formulaire            integer                                                                  not null
        constraint autoform_formulaire_instance_autoform_formulaire_id_fk references unicaen_autoform_formulaire (id)
        on delete cascade,
    histo_creation        timestamp                                                                not null,
    histo_createur_id     integer                                                                  not null
        constraint composante_createur_fk references unicaen_utilisateur_user,
    histo_modification    timestamp                                                                not null,
    histo_modificateur_id integer                                                                  not null
        constraint composante_modificateur_fk references unicaen_utilisateur_user,
    histo_destruction     timestamp,
    histo_destructeur_id  integer
        constraint composante_destructeur_fk references unicaen_utilisateur_user
);
create unique index autoform_formulaire_instance_id_uindex
    on unicaen_autoform_formulaire_instance (id);

create table unicaen_autoform_formulaire_reponse
(
        id                serial not null
        constraint autoform_reponse_pk primary key,
    instance              integer                                                                 not null
        constraint autoform_formulaire_reponse_instance_fk references unicaen_autoform_formulaire_instance
        on delete cascade,
    champ                 integer                                                                 not null
        constraint autoform_reponse_champ_fk references unicaen_autoform_champ
        on delete cascade,
    reponse               text,
    histo_creation        timestamp                                                               not null,
    histo_createur_id     integer                                                                 not null
        constraint composante_createur_fk references unicaen_utilisateur_user,
    histo_modification    timestamp                                                               not null,
    histo_modificateur_id integer                                                                 not null
        constraint composante_modificateur_fk references unicaen_utilisateur_user,
    histo_destruction     timestamp,
    histo_destructeur_id  integer
        constraint composante_destructeur_fk references unicaen_utilisateur_user
);

create unique index autoform_reponse_id_uindex on unicaen_autoform_formulaire_reponse (id);