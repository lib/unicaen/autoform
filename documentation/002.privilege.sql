INSERT INTO unicaen_privilege_categorie (code, libelle, ordre, namespace)
VALUES ('autoformindex','Autoform - Gestion de l''index',5000,'UnicaenAutoform\Provider\Privilege');
INSERT INTO unicaen_privilege_privilege(CATEGORIE_ID, CODE, LIBELLE, ORDRE)
WITH d(code, lib, ordre) AS (
    SELECT 'index', 'Afficher le menu', 10
)
SELECT cp.id, d.code, d.lib, d.ordre
FROM d
JOIN unicaen_privilege_categorie cp ON cp.CODE = 'autoformindex';

INSERT INTO unicaen_privilege_categorie (code, libelle, ordre, namespace)
VALUES ('autoformformulaire','Autoform - Gestion des formulaires',5100,'UnicaenAutoform\Provider\Privilege');
INSERT INTO unicaen_privilege_privilege(CATEGORIE_ID, CODE, LIBELLE, ORDRE)
WITH d(code, lib, ordre) AS (
    SELECT 'formulaire_index', 'Accéder à l''index', 10 UNION
    SELECT 'formulaire_afficher', 'Afficher', 20 UNION
    SELECT 'formulaire_ajouter', 'Ajouter', 30 UNION
    SELECT 'formulaire_modifier', 'Modifier', 40 UNION
    SELECT 'formulaire_historiser', 'Historiser/Restaurer', 50 UNION
    SELECT 'formulaire_supprimer', 'Supprimer', 60
)
SELECT cp.id, d.code, d.lib, d.ordre
FROM d
JOIN unicaen_privilege_categorie cp ON cp.CODE = 'autoformformulaire';

INSERT INTO unicaen_privilege_categorie (code, libelle, ordre, namespace)
VALUES ('autoformcategorie','Autoform - Gestion des catégories',5200,'UnicaenAutoform\Provider\Privilege');
INSERT INTO unicaen_privilege_privilege(CATEGORIE_ID, CODE, LIBELLE, ORDRE)
WITH d(code, lib, ordre) AS (
    SELECT 'categorief_index', 'Accéder à l''index', 10 UNION
    SELECT 'categorief_afficher', 'Afficher', 20 UNION
    SELECT 'categorief_ajouter', 'Ajouter', 30 UNION
    SELECT 'categorief_modifier', 'Modifier', 40 UNION
    SELECT 'categorief_historiser', 'Historiser/Restaurer', 50 UNION
    SELECT 'categorief_supprimer', 'Supprimer', 60
)
SELECT cp.id, d.code, d.lib, d.ordre
FROM d
JOIN unicaen_privilege_categorie cp ON cp.CODE = 'autoformcategorie';

INSERT INTO unicaen_privilege_categorie (code, libelle, ordre, namespace)
VALUES ('autoformchamp','Autoform - Gestion des champs',5300,'UnicaenAutoform\Provider\Privilege');
INSERT INTO unicaen_privilege_privilege(CATEGORIE_ID, CODE, LIBELLE, ORDRE)
WITH d(code, lib, ordre) AS (
    SELECT 'champ_index', 'Accéder à l''index', 10 UNION
    SELECT 'champ_afficher', 'Afficher', 20 UNION
    SELECT 'champ_ajouter', 'Ajouter', 30 UNION
    SELECT 'champ_modifier', 'Modifier', 40 UNION
    SELECT 'champ_historiser', 'Historiser/Restaurer', 50 UNION
    SELECT 'champ_supprimer', 'Supprimer', 60
)
SELECT cp.id, d.code, d.lib, d.ordre
FROM d
JOIN unicaen_privilege_categorie cp ON cp.CODE = 'autoformchamp';

INSERT INTO unicaen_privilege_categorie (code, libelle, ordre, namespace)
VALUES ('champtype','Autoform - Gestion des type de champs',5400,'UnicaenAutoform\Provider\Privilege');
INSERT INTO unicaen_privilege_privilege(CATEGORIE_ID, CODE, LIBELLE, ORDRE)
WITH d(code, lib, ordre) AS (
    SELECT 'champtype_index', 'Accéder à l''index', 10 UNION
    SELECT 'champtype_afficher', 'Afficher', 20 UNION
    SELECT 'champtype_modifier', 'Modifier', 30
)
SELECT cp.id, d.code, d.lib, d.ordre
FROM d
JOIN unicaen_privilege_categorie cp ON cp.CODE = 'champtype';

